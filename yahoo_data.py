from pandas_datareader import data as pdr
from datetime import date, datetime, timedelta, timezone
import yfinance as yf
yf.pdr_override()
import pandas as pd
from time import mktime
# import pytz
import requests
import quandl
import sys,getopt,argparse
import configparser


class yahooFinance:
    def __init__(self, ticker, start, end, corp_actions=False, save_in_file=False, interval='1d'):
        self.ticker = ticker  # please pass with a list
        self.start = start
        self.end = end
        self.start_date = datetime.strptime(self.start, '%Y%m%d')
        self.end_date = datetime.strptime(self.end, '%Y%m%d')
        self.today = date.today()
        self.corp_actions = corp_actions
        self.save_in_file = save_in_file
        self.interval = interval
        self.config = configparser.ConfigParser()
        self.config.read('config.ini')

    def get_data(self, ticker, path=''):
        # print(ticker)
        if self.corp_actions:
            data = pdr.get_data_yahoo(ticker, start=self.start_date, end=self.end_date, actions='True')
        else:
            data = pdr.get_data_yahoo(ticker, start=self.start_date, end=self.end_date)
        if self.save_in_file:
            dataname = ticker + '_' + str(self.today) + '.csv'
            self.save_data(data, path + dataname)
            print(f"The Data is saved in {path}{dataname}")
        else:
            return data

    def save_data(self, df, filename):
        df.to_csv(filename)

    def yahoo_API_pandas(self,path=''):
        """
        If your code uses pandas_datareader and you want to download data faster,
        you can “hijack” pandas_datareader.data.get_data_yahoo() method to use yfinance
        while making sure the returned data is in the same format
        as pandas_datareader’s get_data_yahoo().
        :return:
        """
        for tik in self.ticker:
            print(self.get_data(tik,path))

    def convert_date_to_epoch(self, date):
        """
        The date should be provided in YYYYMMDD format
        Seperate handle needs to be done before and after 19700101
        :param date:
        :return: epoch
        """
        # FIX ME  add timeZone (19700101)
        dt = datetime.strptime(date, '%Y%m%d')
        break_date = datetime.strptime('19700101', '%Y%m%d')
        if dt > break_date:
            return int(mktime(dt.timetuple()))
        else:
            diff_day = (break_date - dt).days
            parallel_date = break_date + timedelta(days=diff_day)
            return -int(mktime(parallel_date.timetuple()))

    def request_yahoo_api(self, event, path = ''):
        """
        https://query1.finance.yahoo.com/v7/finance/quote?lang=en-US&region=US&corsDomain=finance.yahoo.com&fields=symbol,longName,shortName,priceHint,regularMarketPrice,regularMarketChange,regularMarketChangePercent,currency,regularMarketTime,regularMarketVolume,quantity,averageDailyVolume3Month,regularMarketDayHigh,regularMarketDayLow,regularMarketPrice,regularMarketOpen,fiftyTwoWeekHigh,fiftyTwoWeekLow,regularMarketPrice,regularMarketOpen,sparkline,marketCap&symbols=000001.SS,600600.SS
        :param event:div, history, splt
        :return:
        """
        start = self.convert_date_to_epoch(self.start)
        end = self.convert_date_to_epoch(self.end)
        # urllink = 'https://query1.finance.yahoo.com/v7/finance/download/GE?period1=1550620800&period2=1588896000&interval=1d&events=split'
        for stock in self.ticker:
            urllink = 'https://query1.finance.yahoo.com/v7/finance/download/{stock}?period1={start}&period2={end}&interval={interval}&events={event}'.format(
                stock=stock, start=start, end=end, interval=self.interval, event=event)

            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36'}

            r = requests.get(urllink, headers=headers).text
            if not self.save_in_file:
                print(stock, ': ', r)
            else:
                dataname = stock + '_' + event + '_' + str(self.today) + '.csv'
                with open(path + dataname, "w") as f:
                    for i in r:
                        f.write(str(i))
                print(f"The Data is saved in {path}{dataname}")

    def yahoo_API_python_get_description(self, long=False):
        """
        long=True shows long description, if we run without long argumnet or long=False it will show  short description
        https://aroussi.com/post/python-yahoo-finance
         yfinance   wraps the new Yahoo Finance API .
        :return:
        """
        if not long:
            for stk in self.ticker:
                try:
                    stock = yf.Ticker(stk)
                    info = stock.info
                    print(stk, 'website: ', info['website'], 'country: ', info['country'], 'currency: ', info['currency'],
                          'exchange: ', info['exchange'], 'shortName: ', info['shortName'], 'quoteType: ', info['quoteType'],
                          'sharesOutstanding: ', info['sharesOutstanding'])
                except IndexError:
                    pass
        else:
            for stk in self.ticker:
                try:
                    stock = yf.Ticker(stk)
                    info = stock.info
                    print(info)
                except IndexError:
                    pass

    def yahoo_API_python_get_shares_outstanding(self, path=''):
        dataname = 'SHO' + '_CSE_' + str(self.today) + '.csv'
        f = open(path + dataname, "w")
        for stk in self.ticker:
            try:
                print(stk)
                stock = yf.Ticker(stk)
                info = stock.info
                print(stk, ":" , 'sharesOutstanding: ', info['sharesOutstanding'])
                f.write(str(stk))
                f.write(str(":"))
                f.write('sharesOutstanding: ')
                f.write(str(info['sharesOutstanding']))
                f.write('\n')
            except IndexError:
                pass
# this I don't use noW
    def yahoo_API_get_shares_outstanding(self, path=''):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36'}
        if  self.save_in_file:
            dataname = 'SHO_' + str(self.today) + '.csv'
            f = open(path + dataname, "w")
            for stock in self.ticker:
                try:
                    print(stock)
                    urllink = 'https://query1.finance.yahoo.com/v7/finance/quote?symbols={stock}'.format(stock=stock)
                    r = requests.get(urllink, headers=headers).json()
                    sho = str(r['quoteResponse']['result'][0]['sharesOutstanding'])
                    print(' SharesOutstanding: ', sho)
                    f.write(str(stock))
                    f.write(str(":"))
                    f.write('sharesOutstanding: ')
                    f.write(sho)
                    f.write('\n')
                    # print(f"The Data is saved in {path} {dataname}")
                except:
                    pass
            print(f"The Data is saved in {path}{dataname}")
        else:
            for stock in self.ticker:
                urllink = 'https://query1.finance.yahoo.com/v7/finance/quote?symbols={stock}'.format(stock=stock)
                r = requests.get(urllink, headers=headers).json()
                print(stock, ': ', r['quoteResponse']['result'][0]['sharesOutstanding'])

    def yahoo_API_python_get_OHLCV(self, path=''):
        for stk in self.ticker:
            stock = yf.Ticker(stk)
            OHLCV = stock.history(start=self.start_date,end=self.end_date)
            if self.save_in_file:
                dataname = stk + '_' + str(self.today) + '.csv'
                OHLCV.to_csv(path + dataname)
                print(f"The Data is saved in {path}{dataname}")
            else:
                # print(OHLCV.head())
                print(stk, ': ', OHLCV)

    def yahoo_API_python_get_dividends(self, path=''):
        """
        API returns all the dividends - we can't specify time range
        But for initialyzing yahooFinance object please give start, end dates
        :param path:
        :return:
        """
        for stk in self.ticker:
            stock = yf.Ticker(stk)
            dividend = stock.dividends
            if self.save_in_file:
                dataname = stk + '_'+ 'all_dividends'+ '_' + str(self.today) + '.csv'
                dividend.to_csv(path + dataname)
                print(f"The Data is saved in {path}{dataname}")
            else:
                # print(OHLCV.head())
                print(stk, ': ', dividend)

    def yahoo_API_python_get_splits(self, path=''):
        """
        API returns all the dividends - we can't specify time range
        But for initialyzing yahooFinance object please give start, end dates
        :param path:
        :return:
        """
        for stk in self.ticker:
            stock = yf.Ticker(stk)
            split = stock.splits
            if self.save_in_file:
                dataname = stk + '_'+ 'all_splits'+ '_' + str(self.today) + '.csv'
                split.to_csv(path + dataname)
                print(f"The Data is saved in {path}{dataname}")
            else:
                # print(OHLCV.head())
                print(stk, ': ', split)

    def quandl_python_API_OHLCV(self, source='WIKI/PRICES', path=''): #ZACKS/FC (commented out data)
        """
        OHLCV data is untill  2018
        :param source:
        :param path:
        :return:
        """
        quandl.ApiConfig.api_key = self.config['quandl']['api_key']
        # data = quandl.Datatable('ZACKS/FC').data()
        # data2 = quandl.Datatable('ZACKS/FC').data(params={'qopts': {'cursor_id': data.meta['next_cursor_id']}})
        # print(data2)
        start_other_format = self.start[0:4] + '-' + self.start[4:6] + '-' + self.start[6:8]
        end_other_format = self.end[0:4] + '-' + self.end[4:6] + '-' + self.end[6:8]
        data = quandl.get_table(source, ticker = self.ticker,
                            # qopts = { 'columns': ['ticker', 'date', 'adj_close'] },
                            date={'gte': start_other_format, 'lte': end_other_format},
                            paginate=True)
        # data = quandl.get_table(source, ticker = self.ticker)
        if self.save_in_file:
            dataname = 'quandl_OHLCV' + '_' + str(self.today) + '.csv'
            data.to_csv(path + dataname)
            print(f"The Data is saved in {path}{dataname}")
        else:
            # print(OHLCV.head())
            print(data)

    def quandl_python_API(self, source='ZACKS/FC', path=''):  # ZACKS/FC (commented out data)
        """
        OHLCV data is untill  2018
        :param source:
        :param path:
        :return:
        """
        quandl.ApiConfig.api_key = self.config['quandl']['api_key']
        # data = quandl.Datatable('ZACKS/FC').data()
        # data2 = quandl.Datatable('ZACKS/FC').data(params={'qopts': {'cursor_id': data.meta['next_cursor_id']}})
        # print(data2)
        # start_other_format = self.start[0:4] + '-' + self.start[4:6] + '-' + self.start[6:8]
        # end_other_format = self.end[0:4] + '-' + self.end[4:6] + '-' + self.end[6:8]
        # data = quandl.get_table(source, ticker=self.ticker,
        #                         # qopts = { 'columns': ['ticker', 'date', 'adj_close'] },
        #                         date={'gte': start_other_format, 'lte': end_other_format},
        #                         paginate=True)
        data = quandl.get_table(source, ticker = self.ticker)
        if self.save_in_file:
            dataname = 'quandl' + '_' + str(self.today) + '.csv'
            data.to_csv(path + dataname)
            print(f"The Data is saved in {path}{dataname}")
        else:
            # print(OHLCV.head())
            print(data)


#https://blog.quandl.com/api-for-futures-data
#https://help.yahoo.com/kb/exchanges-data-providers-yahoo-finance-sln2310.html

if __name__ == '__main__':
    # print(yahoo_crowler('https://finance.yahoo.com/most-active'))
    # tickers = ["TWX-X.CN"]
    # tickers = ["IBM", "AAPL"]
    # tickers = ["ABCS.CN","ABJ.CN","ACG.CN","ACRG/U.CN","ADX.CN","AFD.CN","AFI.CN","AGN.CN","AGRA.CN","AGRO.CN","AG/UN.CN","AHG.CN","AJN.CN","ALTM.CN","AMPD.CN","AMS.CN","AOC.CN","API.CN","APP.CN","AREV.CN","ARM.CN","ARO.CN","ARQ.CN","ASE.CN","ASIA.CN","ASTI.CN","ATHR.CN","ATT.CN","AUAG.CN","AUSA.CN","AVM/X.CN","AWR.CN","AYL.CN","AYR/A.CN","BAC.CN","BAMM.CN","BBM.CN","BBR.CN","BBT.CN","BCA.CN","BCFN.CN","BCX.CN","BDR.CN","BE.CN","BEAN.CN","BETR.CN","BEV.CN","BFG.CN","BGRD/X.CN","BHNG.CN","BHSC.CN","BIGG.CN","BILZ.CN","BIO.CN","BJB.CN","BKS.CN","BLGV.CN","BLK.CN","BLLG.CN","BLO.CN","BLR.CN","BNKR.CN","BOLT.CN","BOSS.CN","BRH.CN","BTC.CN","BTC/PR/A.CN","BULL.CN","BUX.CN","BUZZ.CN","BXV.CN","BXXX.CN","BYRN.CN","CALI.CN","CANA.CN","CANN.CN","CAT.CN","CBDN.CN","CBDT.CN","CBII.CN","CBIS.CN","CBK.CN","CBP.CN","CCC.CN","CCHW.CN","CCR.CN","CDN.CN","CDPR.CN","CDVA.CN","CEG.CN","CFE.CN","CGM.CN","CGN.CN","CGOC.CN","CGRO.CN","CHM.CN","CHOO.CN","CHV.CN","CIG.CN","CIM.CN","CK.CN","CL.CN","CLSH.CN","CMC.CN","CME.CN","CNFA.CN","CNI.CN","CNNA.CN","CNTR.CN","CO.CN","CODE.CN","COOL.CN","CPTR.CN","CRC.CN","CRES.CN","CRFT.CN","CRL.CN","CRS.CN","CRUZ.CN","CRY.CN","CSI.CN","CSQ.CN","CUBE.CN","CURA.CN","CURE.CN","CVGR.CN","CXC.CN","CXXI.CN","CYN.CN","CZC.CN","DCSI.CN","DDI.CN","DEEP.CN","DFLY.CN","DFT.CN","DHC.CN","DIGI.CN","DIXI/U.CN","DNI.CN","DOC.CN","DOSE.CN","DST.CN","DVR.CN","EASY.CN","EAT.CN","EHC.CN","EI.CN","EMER.CN","EOM.CN","EPIC.CN","EPY.CN","ERKA.CN","ERTH.CN","ETI.CN","ETR.CN","EURO.CN","EVG.CN","EXM.CN","EZNC.CN","FANS.CN","FAT.CN","FDM.CN","FE.CN","FENX.CN","FFNT.CN","FFT.CN","FLO.CN","FLYY.CN","FMAN.CN","FNAU.CN","FNQ.CN","FOG.CN","FONE.CN","FOX.CN","GABY.CN","GAIA.CN","GBC.CN","GBLC.CN","GBLK.CN","GBRX.CN","GCA.CN","GCIT.CN","GECC.CN","GENI.CN","GENM.CN","GFI.CN","GGB.CN","GGC.CN","GHG.CN","GL.CN","GLH.CN","GLL.CN","GLM.CN","GOCO.CN","GOOP.CN","GOR.CN","GPK.CN","GPMI.CN","GRE.CN","GRIN.CN","GSK.CN","GSTR.CN","GTBE.CN","GTCH.CN","GTII.CN","GTI/X.CN","HARV.CN","HARY.CN","HBOR.CN","HEY.CN","HFH.CN","HGH.CN","HGP.CN","HHS.CN","HITI.CN","HLTH.CN","HODL.CN","HOLL.CN","HP.CN","HS.CN","HUGE.CN","HYPR.CN","IAN.CN","IBAT.CN","IC.CN","ICAN.CN","IDK.CN","IGN.CN","IM.CN","IMCC.CN","IMCX.CN","IME.CN","INDS.CN","INNO.CN","INTL.CN","IONC.CN","IP.CN","IPOT.CN","IRV.CN","ISFT.CN","ISH.CN","ISM.CN","ISOL.CN","ITKO.CN","IZO.CN","J.CN","JANE.CN","JBR.CN","JDF.CN","JJJ/X.CN","JNC.CN","JUSH.CN","KBEV.CN","KDZ/X.CN","KMI.CN","KNR.CN","KWG.CN","KWG/A.CN","LAI.CN","LBM.CN","LECR.CN","LHS.CN","LIB.CN","LILY.CN","LION.CN","LLL.CN","LLP.CN","LLT.CN","LNK.CN","LOAN.CN","LOVE.CN","LUFF.CN","LUX.CN","LUXX.CN","LXX.CN","MAKA.CN","MANN.CN","MCL.CN","MDD.CN","MEC.CN","MGRO.CN","MHY/UN.CN","MICH.CN","MIRL.CN","MIS.CN","MJ.CN","MJAR.CN","MJRX.CN","MMAT.CN","MMC.CN","MMEN.CN","MMF/UN.CN","MMI.CN","MMJ.CN","MNG.CN","MONT.CN","MOTA.CN","MPV.CN","MPXI.CN","MRBL.CN","MREY.CN","MRM.CN","MVMD.CN","MVT.CN","MWM.CN","MXT.CN","MY.CN","MYM.CN","MYR.CN","NAB.CN","NBUD.CN","NC.CN","NERD.CN","NETC/X.CN","NGW.CN","NHS.CN","NI.CN","NLB.CN","NLR.CN","NLV.CN","NOM.CN","NPT.CN","NSAU.CN","NSG.CN","NSHS.CN","NTAR.CN","NTM.CN","NUGT.CN","NUR.CN","NVG.CN","NWI.CN","NXU.CN","OIL.CN","OILS.CN","OPC.CN","OPI.CN","ORCD.CN","ORGN.CN","ORI.CN","ORTH.CN","OSO.CN","OVAT.CN","OWLI.CN","PACR.CN","PAID.CN","PAU.CN","PBIT.CN","PBT/U.CN","PDO.CN","PDTI.CN","PERK.CN","PGOL.CN","PIKE.CN","PILL.CN","PJO.CN","PKG.CN","PKK.CN","PLAY.CN","PLTH.CN","PLUS.CN","PMC.CN","PMED.CN","POT.CN","PREV.CN","PRMO.CN","PRT.CN","PSE.CN","PTX.CN","PUMP.CN","PUSH.CN","PWR.CN","QBOT.CN","QCA.CN","QMI.CN","QQ.CN","QSC.CN","RAD.CN","RAIN.CN","RAMM.CN","RAYN.CN","RCLF.CN","RDKO.CN","RELA.CN","REO.CN","RFR.CN","RGLD.CN","RGO/X.CN","RICH.CN","RISE.CN","RIW.CN","RKS.CN","RLG.CN","RLSC.CN","RLTY/U.CN","RMMI.CN","RNG.CN","RNR.CN","ROMJ.CN","RQB.CN","RVR.CN","RVV.CN","RXM.CN","RZR.CN","SAFE.CN","SBD.CN","SCV.CN","SE.CN","SENS.CN","SG.CN","SGRO.CN","SHP.CN","SHRC.CN","SHRM.CN","SHV.CN","SIG.CN","SIRE.CN","SIX.CN","SIXW.CN","SKRB.CN","SKYL.CN","SLNG.CN","SLZ.CN","SNA.CN","SNL.CN","SNN.CN","SOFT.CN","SOL.CN","SONA.CN","SOW.CN","SP.CN","SPEY.CN","SPFY.CN","SPMT.CN","SPO.CN","SPOR.CN","SPR.CN","SQID.CN","SQX.CN","SSVR.CN","ST.CN","STEM.CN","STIL.CN","STX.CN","SUN.CN","SUV.CN","SWIS.CN","SX.CN","SXTY.CN","SXX.CN","TAAL.CN","TAI.CN","TAK.CN","TCAN.CN","TCF.CN","TCI.CN","TE.CN","TEMP.CN","TENO.CN","TEQ.CN","TER.CN","TFC.CN","TGC.CN","TGIF.CN","THC.CN","TILT.CN","TIUM/U.CN","TKR.CN","TLP/UN.CN","TMAS.CN","TMED.CN","TN.CN","TNY.CN","TO.CN","TOC.CN","TOKI.CN","TPS.CN","TRG.CN","TRUL.CN","TSK.CN","TTT.CN","TTX.CN","TUSK.CN","TWLV.CN","TWX-X.CN","UAV.CN","UBM.CN","UBQ.CN","UP.CN","UPCO.CN","URB.CN","URB/A.CN","URL.CN","USGD.CN","VAI.CN","VC.CN","VCAN.CN","VCT-X.CN","VENI.CN","VEXT.CN","VIBE.CN","VIDA.CN","VIN.CN","VP.CN","VPH.CN","VPN.CN","VR.CN","VREO.CN","VRT.CN","VS.CN","VSBY.CN","VST.CN","VVV.CN","VYGR.CN","WAL.CN","WAYL.CN","WBIO.CN","WGC.CN","WIFI.CN","WIKI.CN","WPN.CN","WRW.CN","WSM-X.CN","WTER.CN","WUC.CN","XBLK.CN","XCX.CN","XMG.CN","XOP.CN","XPHY.CN","XRO.CN","XRX.CN","XS.CN","XTRX.CN","YT.CN","ZAIR.CN","ZBR.CN","ZEU.CN","ZNK.CN","ZRO.CN","ZTE.CN","ZX.CN"]
    # tickers = ["AAAA.NE","AA.NE","AAB.NE","AAC.NE","AAD.NE","AAG.NE","AAL.NE","AAM.NE","AAN.NE","AAP.NE","A.NE","AAT.NE","AAV.NE","AAX.NE","AAZ.NE","ABC.NE","ABCN.NE","ABCS.NE","ABE.NE","ABI.NE","ABJ.NE","ABM.NE","ABN.NE","ABRA.NE","ABR.NE","ABT.NE","ABX.NE","ABZ.NE","AC.NE","ACB.NE","ACD.NE","ACES.NE","ACG.NE","ACH.NE","ACI.NE","ACP.NE","ACQ.NE","ACS.NE","ACST.NE","ACU.NE","ACZ.NE","AD.NE","ADCO.NE","ADD.NE","ADE.NE","ADG.NE","ADK.NE","ADL.NE","ADN.NE","ADP.NE","ADV.NE","ADVZ.NE","ADX.NE","ADYA.NE","ADZ.NE","ADZN.NE","AEC.NE","AEF.NE","AEM.NE","AEN.NE","AEP.NE","AET.NE","AEX.NE","AEZ.NE","AEZS.NE","AF.NE","AFCC.NE","AFD.NE","AFE.NE","AFF.NE","AFI.NE","AFM.NE","AFN.NE","AFR.NE","AGB.NE","AGC.NE","AGD.NE","AGG.NE","AGH.NE","AGI.NE","AGL.NE","AGLD.NE","AGM.NE","AGN.NE","AGO.NE","AGRA.NE","AGRL.NE","AGRO.NE","AGT.NE","AGU.NE","AGZ.NE","AHE.NE","AHG.NE","AHI.NE","AHP.NE","AHR.NE","AHS.NE","AHU.NE","AI.NE","AIF.NE","AII.NE","AIIM.NE","AIM.NE","AIML.NE","AIN.NE","AIR.NE","AIX.NE","AJN.NE","AJX.NE","AKE.NE","AKG.NE","AKH.NE","AKR.NE","AKU.NE","ALA.NE","AL.NE","ALB.NE","ALC.NE","ALD.NE","ALDE.NE","ALEF.NE","ALG.NE","ALI.NE","ALLI.NE","ALM.NE","ALO.NE","ALP.NE","ALQ.NE","ALS.NE","ALTA.NE","ALT.NE","ALTM.NE","ALTS.NE","ALV.NE","ALYA.NE","ALY.NE","ALZ.NE","AM.NE","AMC.NE","AME.NE","AMH.NE","AMI.NE","AMK.NE","AML.NE","AMM.NE","AMO.NE","AMPD.NE","AMR.NE","AMS.NE","AMV.NE","AMX.NE","AMY.NE","AMZ.NE","AN.NE","ANB.NE","AND.NE","ANE.NE","ANF.NE","ANG.NE","ANK.NE","ANTL.NE","ANX.NE","ANZ.NE","AOC.NE","AOI.NE","AORO.NE","AOT.NE","APC.NE","APHA.NE","API.NE","APL.NE","APLI.NE","APN.NE","APO.NE","APP.NE","APPX.NE","APS.NE","APV.NE","APX.NE","APY.NE","AQA.NE","AQ.NE","AQN.NE","AQS.NE","AQUA.NE","ARA.NE","AR.NE","ARB.NE","ARC.NE","ARCH.NE","ARD.NE","ARE.NE","AREV.NE","ARG.NE","ARH.NE","ARIC.NE","ARK.NE","ARM.NE","ARO.NE","ARQ.NE","ARR.NE","ART.NE","ARTG.NE","ARU.NE","ARX.NE","ARY.NE","ARZ.NE","ASE.NE","ASG.NE","ASIA.NE","ASI.NE","ASL.NE","ASM.NE","ASN.NE","ASND.NE","ASNT.NE","ASO.NE","ASP.NE","ASQ.NE","ASR.NE","AST.NE","ASTI.NE","ATA.NE","AT.NE","ATC.NE","ATE.NE","ATG.NE","ATH.NE","ATHR.NE","ATI.NE","ATL.NE","ATM.NE","ATP.NE","ATT.NE","ATU.NE","ATV.NE","ATW.NE","ATX.NE","ATY.NE","ATZ.NE","AUAG.NE","AU.NE","AUAU.NE","AUCU.NE","AUEN.NE","AUG.NE","AUL.NE","AUMB.NE","AUN.NE","AUP.NE","AURX.NE","AUSA.NE","AUT.NE","AUU.NE","AUX.NE","AVA.NE","AVCN.NE","AVCR.NE","AVE.NE","AVG.NE","AVK.NE","AVL.NE","AVM.NE","AVN.NE","AVO.NE","AVP.NE","AVU.NE","AVX.NE","AWE.NE","AWI.NE","AWR.NE","AWS.NE","AWX.NE","AXA.NE","AXC.NE","AXE.NE","AXIS.NE","AXL.NE","AXM.NE","AXN.NE","AXQ.NE","AXR.NE","AXS.NE","AXU.NE","AXV.NE","AXY.NE","AYA.NE","AYL.NE","AYM.NE","AZ.NE","AZM.NE","AZR.NE","AZS.NE","AZT.NE","AZX.NE","AZZ.NE","BAA.NE","BABY.NE","BAC.NE","BAD.NE","BAN.NE","BANC.NE","BANK.NE","B.NE","BAR.NE","BARI.NE","BASE.NE","BAT.NE","BATT.NE","BAU.NE","BAXS.NE","BAY.NE","BB.NE","BBB.NE","BBI.NE","BBLS.NE","BBM.NE","BBR.NE","BBT.NE","BCA.NE","BCB.NE","BCC.NE","BCE.NE","BCF.NE","BCFN.NE","BCI.NE","BCK.NE","BCM.NE","BCN.NE","BCP.NE","BCT.NE","BCU.NE","BCX.NE","BDG.NE","BDGC.NE","BDI.NE","BDIV.NE","BDR.NE","BDT.NE","BEA.NE","BEAN.NE","BE.NE","BEE.NE","BEER.NE","BEL.NE","BEN.NE","BES.NE","BETR.NE","BEV.NE","BEW.NE","BEX.NE","BEY.NE","BFD.NE","BFF.NE","BFIN.NE","BGA.NE","BGC.NE","BGE.NE","BGF.NE","BGG.NE","BGL.NE","BGM.NE","BGRD.NE","BGS.NE","BGU.NE","BHAV.NE","BHC.NE","BHK.NE","BHNG.NE","BHR.NE","BHS.NE","BHSC.NE","BHT.NE","BHV.NE","BIGG.NE","BILZ.NE","BIO.NE","BIPC.NE","BIR.NE","BIS.NE","BIT.NE","BITF.NE","BITK.NE","BJB.NE","BK.NE","BKCH.NE","BKD.NE","BKI.NE","BKL.NE","BKM.NE","BKMT.NE","BKR.NE","BKS.NE","BKX.NE","BLCK.NE","BLD.NE","BLDP.NE","BLGV.NE","BLIS.NE","BLK.NE","BLLG.NE","BLM.NE","BLN.NE","BLO.NE","BLOC.NE","BLOK.NE","BLOV.NE","BLOX.NE","BLR.NE","BLS.NE","BLU.NE","BLUE.NE","BLVD.NE","BLX.NE","BM.NE","BME.NE","BMET.NE","BMG.NE","BMK.NE","BML.NE","BMM.NE","BMO.NE","BMX.NE","BNC.NE","BNCH.NE","BND.NE","BNE.NE","BNG.NE","BNP.NE","BNR.NE","BNS.NE","BOC.NE","BOG.NE","BOL.NE","BOLT.NE","BOR.NE","BOS.NE","BOSS.NE","BOW.NE","BOY.NE","BP.NE","BPE.NE","BPL.NE","BPLI.NE","BPRF.NE","BPX.NE","BQE.NE","BRAG.NE","BR.NE","BRB.NE","BRC.NE","BRD.NE","BREA.NE","BRE.NE","BRH.NE","BRIO.NE","BRL.NE","BRM.NE","BRO.NE","BRS.NE","BRTL.NE","BRU.NE","BRY.NE","BRZ.NE","BSC.NE","BSH.NE","BSI.NE","BSK.NE","BSR.NE","BST.NE","BSX.NE","BTC.NE","BTE.NE","BTH.NE","BTI.NE","BTL.NE","BTO.NE","BTR.NE","BTRU.NE","BTT.NE","BTU.NE","BTV.NE","BU.NE","BUD.NE","BUDD.NE","BUF.NE","BUI.NE","BUL.NE","BULL.NE","BUS.NE","BUX.NE","BUZZ.NE","BVA.NE","BVO.NE","BWLK.NE","BWR.NE","BX.NE","BXE.NE","BXF.NE","BXR.NE","BXV.NE","BXXX.NE","BYD.NE","BYL.NE","BYN.NE","BYU.NE","BZ.NE","BZI.NE","CACB.NE","CAD.NE","CAE.NE","CAF.NE","CAFR.NE","CAG.NE","CAGG.NE","CAGS.NE","CAI.NE","CAL.NE","CALI.NE","CALL.NE","CAM.NE","CANA.NE","CAN.NE","CANB.NE","CAND.NE","CANN.NE","CANX.NE","CAO.NE","CAP.NE","CAPS.NE","CARA.NE","CARS.NE","CASA.NE","CAS.NE","CAT.NE","CAV.NE","CAY.NE","CBA.NE","CBDN.NE","CBDT.NE","CBE.NE","CBG.NE","CBH.NE","CBI.NE","CBII.NE","CBIS.NE","CBK.NE","CBL.NE","CBLT.NE","CBLU.NE","CBO.NE","CBP.NE","CBQ.NE","CBR.NE","CBS.NE","CBV.NE","CBW.NE","CBX.NE","CCA.NE","CCB.NE","CCC.NE","CCD.NE","CCE.NE","CCHW.NE","CCM.NE","CCO.NE","CCOR.NE","CCR.NE","CCW.NE","CCX.NE","CCZ.NE","CDA.NE","CD.NE","CDB.NE","CDC.NE","CDG.NE","CDH.NE","CDLB.NE","CDN.NE","CDPR.NE","CDU.NE","CDVA.NE","CDV.NE","CDZ.NE","CEA.NE","CE.NE","CEB.NE","CED.NE","CEE.NE","CEF.NE","CEG.NE","CEI.NE","CEM.NE","CEN.NE","CEP.NE","CERV.NE","CES.NE","CESG.NE","CET.NE","CEU.NE","CEW.NE","CF.NE","CFB.NE","CFE.NE","CFF.NE","CFL.NE","CFLX.NE","CFM.NE","CFP.NE","CFW.NE","CFX.NE","CFY.NE","CGAA.NE","CG.NE","CGC.NE","CGD.NE","CGE.NE","CGG.NE","CGI.NE","CGL.NE","CGLD.NE","CGM.NE","CGN.NE","CGO.NE","CGOC.NE","CGP.NE","CGR.NE","CGRO.NE","CGT.NE","CGW.NE","CGX.NE","CGXF.NE","CGY.NE","CH.NE","CHB.NE","CHC.NE","CHEM.NE","CHH.NE","CHM.NE","CHN.NE","CHOO.NE","CHQ.NE","CHR.NE","CHTA.NE","CHV.NE","CHW.NE","CHX.NE","CIA.NE","CIC.NE","CID.NE","CIE.NE","CIF.NE","CIFR.NE","CIG.NE","CIGI.NE","CIM.NE","CIN.NE","CINC.NE","CIO.NE","CIT.NE","CIX.NE","CJ.NE","CJC.NE","CJP.NE","CJT.NE","CK.NE","CKE.NE","CKG.NE","CKI.NE","CKK.NE","CKR.NE","CL.NE","CLB.NE","CLE.NE","CLF.NE","CLG.NE","CLH.NE","CLI.NE","CLIQ.NE","CLM.NE","CLQ.NE","CLR.NE","CLS.NE","CLU.NE","CLV.NE","CLY.NE","CLZ.NE","CMAG.NE","CM.NE","CMAR.NE","CMB.NE","CMC.NE","CMCE.NE","CMD.NE","CME.NE","CMED.NE","CMET.NE","CMEY.NE","CMG.NE","CMH.NE","CMI.NE","CML.NE","CMM.NE","CMMC.NE","CMR.NE","CMS.NE","CMU.NE","CMUE.NE","CMV.NE","CMX.NE","CNA.NE","CN.NE","CNC.NE","CND.NE","CNE.NE","CNFA.NE","CNG.NE","CNH.NE","CNI.NE","CNL.NE","CNNA.NE","CNNX.NE","CNO.NE","CNQ.NE","CNR.NE","CNS.NE","CNT.NE","CNTR.NE","CNVC.NE","CNX.NE","CNZ.NE","CO.NE","COBC.NE","CODE.NE","COG.NE","COIN.NE","COL.NE","COM.NE","COMM.NE","CONA.NE","CONE.NE","COO.NE","COOL.NE","COP.NE","COPR.NE","COPS.NE","COQ.NE","COR.NE","CORE.NE","CORP.NE","CORV.NE","COT.NE","COV.NE","COW.NE","CPA.NE","CP.NE","CPC.NE","CPD.NE","CPG.NE","CPH.NE","CPI.NE","CPL.NE","CPM.NE","CPO.NE","CPS.NE","CPTO.NE","CPTR.NE","CPV.NE","CPX.NE","CQE.NE","CQR.NE","CR.NE","CRB.NE","CRC.NE","CRD.NE","CRDL.NE","CRE.NE","CRED.NE","CRES.NE","CRF.NE","CRFT.NE","CRH.NE","CRL.NE","CRN.NE","CRO.NE","CRON.NE","CROP.NE","CRP.NE","CRQ.NE","CRS.NE","CRU.NE","CRUZ.NE","CRV.NE","CRWN.NE","CRX.NE","CRY.NE","CRYP.NE","CRZ.NE","CS.NE","CSAV.NE","CSC.NE","CSD.NE","CSI.NE","CSK.NE","CSL.NE","CSM.NE","CSO.NE","CSPG.NE","CSQ.NE","CSR.NE","CST.NE","CSTR.NE","CSU.NE","CSV.NE","CSX.NE","CSY.NE","CTA.NE","CT.NE","CTC.NE","CTEC.NE","CTI.NE","CTM.NE","CTN.NE","CTO.NE","CTS.NE","CTU.NE","CTX.NE","CU.NE","CUB.NE","CUBE.NE","CUC.NE","CUDA.NE","CUD.NE","CUG.NE","CUI.NE","CULT.NE","CUM.NE","CUO.NE","CURA.NE","CURE.NE","CUU.NE","CUV.NE","CUX.NE","CUZ.NE","CVA.NE","CVB.NE","CVD.NE","CVE.NE","CVG.NE","CVGR.NE","CVL.NE","CVM.NE","CVR.NE","CVV.NE","CVX.NE","CWB.NE","CWC.NE","CWEB.NE","CWF.NE","CWI.NE","CWL.NE","CWM.NE","CWN.NE","CWO.NE","CWV.NE","CWW.NE","CWX.NE","CX.NE","CXB.NE","CXC.NE","CXF.NE","CXM.NE","CXN.NE","CXO.NE","CXR.NE","CXV.NE","CXX.NE","CXXI.NE","CYB.NE","CYBR.NE","CYBX.NE","CYC.NE","CYF.NE","CYH.NE","CYL.NE","CYM.NE","CYN.NE","CYP.NE","CYX.NE","CZC.NE","CZH.NE","CZN.NE","CZO.NE","CZQ.NE","CZR.NE","CZX.NE","CZY.NE","CZZ.NE","DAC.NE","DAL.NE","DAN.NE","DANC.NE","DAR.NE","DASH.NE","DAU.NE","DB.NE","DBG.NE","DBL.NE","DBO.NE","DBV.NE","DCBO.NE","DCF.NE","DCG.NE","DCM.NE","DCMC.NE","DCOP.NE","DCP.NE","DCS.NE","DCU.NE","DCY.NE","DDB.NE","DDC.NE","DDI.NE","DE.NE","DEC.NE","DEE.NE","DEEP.NE","DEF.NE","DEFN.NE","DELX.NE","DEN.NE","DEX.NE","DF.NE","DFC.NE","DFD.NE","DFE.NE","DFI.NE","DFLY.NE","DFN.NE","DFR.NE","DFS.NE","DFT.NE","DFU.NE","DG.NE","DGC.NE","DGHI.NE","DGO.NE","DGR.NE","DGRC.NE","DGS.NE","DH.NE","DHC.NE","DHR.NE","DHX.NE","DIA.NE","DIAM.NE","DIGI.NE","DISC.NE","DIV.NE","DIVS.NE","DJI.NE","DLC.NE","DLI.NE","DLMA.NE","DLP.NE","DLR.NE","DLRY.NE","DLS.NE","DLTA.NE","DLV.NE","DMA.NE","DM.NE","DME.NE","DMGI.NE","DMI.NE","DML.NE","DMM.NE","DMR.NE","DMX.NE","DNA.NE","DN.NE","DNAX.NE","DNG.NE","DNI.NE","DNT.NE","DNX.NE","DOC.NE","DOL.NE","DOO.NE","DOS.NE","DOSE.NE","DOX.NE","DPC.NE","DPH.NE","DPM.NE","DQD.NE","DQI.NE","DR.NE","DRCU.NE","DRFC.NE","DRFD.NE","DRFE.NE","DRFG.NE","DRFU.NE","DRM.NE","DRMC.NE","DRMU.NE","DRR.NE","DRT.NE","DRV.NE","DRWI.NE","DRX.NE","DS.NE","DSF.NE","DSG.NE","DSM.NE","DSR.NE","DST.NE","DSV.NE","DUG.NE","DVA.NE","DV.NE","DVG.NE","DVI.NE","DVR.NE","DVT.NE","DW.NE","DWG.NE","DWI.NE","DWS.NE","DXA.NE","DXB.NE","DXC.NE","DXD.NE","DXF.NE","DXG.NE","DXI.NE","DXM.NE","DXO.NE","DXP.NE","DXU.NE","DXV.NE","DXX.NE","DXZ.NE","DYA.NE","DYG.NE","DYME.NE","EA.NE","EAC.NE","EAM.NE","E.NE","EARN.NE","EAS.NE","EAST.NE","EASY.NE","EAT.NE","EAU.NE","EBM.NE","EBN.NE","EBY.NE","ECA.NE","ECC.NE","ECI.NE","ECM.NE","ECN.NE","ECO.NE","ECR.NE","ECS.NE","ECT.NE","ECU.NE","EDDY.NE","EDE.NE","EDG.NE","EDGE.NE","EDGF.NE","EDR.NE","EDT.NE","EDV.NE","EEC.NE","EEL.NE","EEV.NE","EFH.NE","EFL.NE","EFN.NE","EFR.NE","EFX.NE","EGA.NE","EGD.NE","EGIF.NE","EGL.NE","EGLD.NE","EGLX.NE","EGM.NE","EGT.NE","EHC.NE","EHE.NE","EHT.NE","EI.NE","EIF.NE","EIL.NE","EKG.NE","EL.NE","ELC.NE","ELD.NE","ELEF.NE","ELF.NE","ELM.NE","ELN.NE","ELO.NE","ELR.NE","ELT.NE","ELV.NE","ELXR.NE","ELY.NE","EMA.NE","EMC.NE","EMER.NE","EMH.NE","EMM.NE","EMN.NE","EMO.NE","EMPX.NE","EMR.NE","EMX.NE","ENA.NE","ENB.NE","ENDP.NE","ENER.NE","ENF.NE","ENG.NE","ENGH.NE","ENL.NE","ENP.NE","ENRG.NE","ENS.NE","ENT.NE","ENW.NE","EOG.NE","EOM.NE","EOX.NE","EP.NE","EPI.NE","EPIC.NE","EPL.NE","EPO.NE","EPS.NE","EPT.NE","EPW.NE","EPWR.NE","EPY.NE","EQ.NE","EQB.NE","EQE.NE","EQG.NE","EQI.NE","EQL.NE","EQTY.NE","EQX.NE","ERA.NE","ER.NE","ERC.NE","ERD.NE","ERE.NE","ERF.NE","ERG.NE","ERKA.NE","ERM.NE","ERN.NE","ERO.NE","ERTH.NE","ERX.NE","ES.NE","ESGA.NE","ESG.NE","ESGB.NE","ESGE.NE","ESGF.NE","ESGG.NE","ESGW.NE","ESGY.NE","ESI.NE","ESK.NE","ESL.NE","ESM.NE","ESN.NE","ESP.NE","ESPT.NE","ESR.NE","ESS.NE","ESU.NE","ESV.NE","ESX.NE","ETAC.NE","ET.NE","ETF.NE","ETG.NE","ETHC.NE","ETHI.NE","ETI.NE","ETMC.NE","ETP.NE","ETR.NE","ETX.NE","EU.NE","EUK.NE","EUO.NE","EUR.NE","EURO.NE","EVA.NE","EV.NE","EVC.NE","EVE.NE","EVER.NE","EVG.NE","EVI.NE","EVM.NE","EVR.NE","EVT.NE","EVX.NE","EW.NE","EWK.NE","EWS.NE","EX.NE","EXC.NE","EXE.NE","EXF.NE","EXG.NE","EXGB.NE","EXGG.NE","EXM.NE","EXN.NE","EXP.NE","EXS.NE","EXX.NE","EYC.NE","EZM.NE","EZNC.NE","FA.NE","FAF.NE","FAI.NE","FAN.NE","FANS.NE","FAO.NE","FAP.NE","F.NE","FAR.NE","FAS.NE","FAT.NE","FBE.NE","FBF.NE","FBU.NE","FBX.NE","FCA.NE","FC.NE","FCC.NE","FCCB.NE","FCCD.NE","FCCL.NE","FCCQ.NE","FCE.NE","FCF.NE","FCGB.NE","FCGI.NE","FCHH.NE","FCHY.NE","FCID.NE","FCIL.NE","FCIQ.NE","FCLH.NE","FCO.NE","FCQH.NE","FCR.NE","FCRH.NE","FCRR.NE","FCSB.NE","FCSW.NE","FCU.NE","FCUD.NE","FCUH.NE","FCUL.NE","FCUQ.NE","FCV.NE","FCW.NE","FCY.NE","FD.NE","FDC.NE","FDE.NE","FDI.NE","FDIV.NE","FDL.NE","FDM.NE","FDV.NE","FDY.NE","FE.NE","FEC.NE","FEN.NE","FENX.NE","FEO.NE","FEX.NE","FF.NE","FFF.NE","FFH.NE","FFN.NE","FFNT.NE","FFOX.NE","FFP.NE","FFT.NE","FG.NE","FGB.NE","FGC.NE","FGD.NE","FGE.NE","FGF.NE","FGH.NE","FGO.NE","FGR.NE","FGX.NE","FHB.NE","FHC.NE","FHD.NE","FHE.NE","FHF.NE","FHG.NE","FHH.NE","FHI.NE","FHM.NE","FHQ.NE","FHU.NE","FI.NE","FIE.NE","FIG.NE","FIL.NE","FINT.NE","FIO.NE","FIRE.NE","FISH.NE","FIVE.NE","FIX.NE","FLAM.NE","FL.NE","FLB.NE","FLCD.NE","FLCI.NE","FLCP.NE","FLDM.NE","FLEM.NE","FLGA.NE","FLGC.NE","FLGD.NE","FLI.NE","FLJA.NE","FLM.NE","FLO.NE","FLOT.NE","FLRM.NE","FLSD.NE","FLSL.NE","FLT.NE","FLUI.NE","FLUR.NE","FLUS.NE","FLWR.NE","FLX.NE","FLY.NE","FMAN.NE","FM.NE","FMC.NE","FMG.NE","FMM.NE","FMN.NE","FMR.NE","FMS.NE","FN.NE","FNAU.NE","FNC.NE","FNQ.NE","FNR.NE","FNV.NE","FO.NE","FOG.NE","FOM.NE","FONE.NE","FOOD.NE","FOR.NE","FORK.NE","FORT.NE","FOUR.NE","FOX.NE","FP.NE","FPC.NE","FPR.NE","FPX.NE","FQC.NE","FR.NE","FRC.NE","FRE.NE","FREE.NE","FRI.NE","FRII.NE","FRK.NE","FRKL.NE","FRN.NE","FRU.NE","FRX.NE","FSB.NE","FSD.NE","FSF.NE","FSL.NE","FSR.NE","FST.NE","FSV.NE","FSW.NE","FSX.NE","FSY.NE","FSZ.NE","FT.NE","FTB.NE","FTEC.NE","FTG.NE","FTI.NE","FTJ.NE","FTN.NE","FTP.NE","FTR.NE","FTS.NE","FTT.NE","FTU.NE","FTY.NE","FUD.NE","FUM.NE","FUND.NE","FURA.NE","FUR.NE","FUSE.NE","FUT.NE","FUU.NE","FVAN.NE","FV.NE","FVI.NE","FVL.NE","FW.NE","FWZ.NE","FXC.NE","FXM.NE","FYL.NE","GABY.NE","GAIA.NE","GAL.NE","GAME.NE","G.NE","GAR.NE","GAS.NE","GAU.NE","GBC.NE","GBE.NE","GBF.NE","GBL.NE","GBLC.NE","GBLK.NE","GBLT.NE","GBML.NE","GBR.NE","GBRX.NE","GBT.NE","GBU.NE","GCA.NE","GC.NE","GCG.NE","GCIT.NE","GCL.NE","GCM.NE","GCN.NE","GCR.NE","GCT.NE","GCX.NE","GDBO.NE","GDC.NE","GDI.NE","GDL.NE","GDM.NE","GDN.NE","GDNP.NE","GDP.NE","GDS.NE","GDV.NE","GDX.NE","GECC.NE","GEI.NE","GEL.NE","GEM.NE","GEMC.NE","GEN.NE","GENE.NE","GENI.NE","GENM.NE","GENX.NE","GEO.NE","GER.NE","GET.NE","GFG.NE","GFI.NE","GFK.NE","GFL.NE","GGA.NE","GG.NE","GGAU.NE","GGB.NE","GGC.NE","GGD.NE","GGG.NE","GGI.NE","GGL.NE","GGM.NE","GGO.NE","GGS.NE","GGX.NE","GH.NE","GHD.NE","GHG.NE","GHL.NE","GHR.NE","GI.NE","GIGA.NE","GIII.NE","GIL.NE","GIS.NE","GIT.NE","GIX.NE","GKX.NE","GL.NE","GLB.NE","GLD.NE","GLDN.NE","GLDX.NE","GLG.NE","GLH.NE","GLI.NE","GLK.NE","GLL.NE","GLM.NE","GLO.NE","GLR.NE","GLW.NE","GLXY.NE","GMA.NE","GMC.NE","GMN.NE","GMP.NE","GMV.NE","GMX.NE","GNC.NE","GNF.NE","GNG.NE","GNH.NE","GNI.NE","GNT.NE","GO.NE","GOCO.NE","GOE.NE","GOG.NE","GOK.NE","GOLD.NE","GOLO.NE","GOM.NE","GOOD.NE","GOOP.NE","GOOS.NE","GOR.NE","GOT.NE","GP.NE","GPC.NE","GPG.NE","GPH.NE","GPK.NE","GPLY.NE","GPM.NE","GPMI.NE","GPR.NE","GPS.NE","GPV.NE","GPY.NE","GQ.NE","GQC.NE","GQM.NE","GRA.NE","GR.NE","GRAT.NE","GRB.NE","GRC.NE","GRCC.NE","GRDM.NE","GRE.NE","GRG.NE","GRI.NE","GRIN.NE","GRK.NE","GRL.NE","GRN.NE","GRO.NE","GROW.NE","GRR.NE","GRSL.NE","GRV.NE","GRZ.NE","GSA.NE","GS.NE","GSC.NE","GSD.NE","GSH.NE","GSI.NE","GSK.NE","GSP.NE","GSPR.NE","GSR.NE","GSS.NE","GSTR.NE","GSV.NE","GSW.NE","GSY.NE","GTA.NE","GT.NE","GTB.NE","GTBE.NE","GTC.NE","GTCH.NE","GTEC.NE","GTG.NE","GTI.NE","GTII.NE","GTMS.NE","GTP.NE","GTR.NE","GTT.NE","GTWO.NE","GTX.NE","GUD.NE","GUG.NE","GUN.NE","GUS.NE","GUY.NE","GV.NE","GVC.NE","GVG.NE","GVR.NE","GVY.NE","GWA.NE","GWM.NE","GWO.NE","GX.NE","GXE.NE","GXL.NE","GXM.NE","GXO.NE","GXR.NE","GXS.NE","GXU.NE","GYA.NE","GYSR.NE","GZD.NE","GZT.NE","GZZ.NE","HAB.NE","HAC.NE","HAD.NE","HADM.NE","HAF.NE","HAJ.NE","HAL.NE","HALO.NE","HAN.NE","HAND.NE","H.NE","HAR.NE","HARC.NE","HARV.NE","HARY.NE","HAU.NE","HAWK.NE","HAZ.NE","HBAL.NE","HBB.NE","HBC.NE","HBD.NE","HBE.NE","HBF.NE","HBG.NE","HBGD.NE","HBK.NE","HBLK.NE","HBM.NE","HBOR.NE","HBP.NE","HBU.NE","HC.NE","HCB.NE","HCBB.NE","HCG.NE","HCN.NE","HCON.NE","HCRE.NE","HDGE.NE","HDI.NE","HEA.NE","HE.NE","HEC.NE","HED.NE","HEE.NE","HEF.NE","HEI.NE","HEJ.NE","HELX.NE","HEM.NE","HEMB.NE","HEMP.NE","HEO.NE","HEP.NE","HER.NE","HERO.NE","HERS.NE","HEU.NE","HEUR.NE","HEW.NE","HEWB.NE","HEX.NE","HEXO.NE","HEY.NE","HFA.NE","HFC.NE","HFD.NE","HFF.NE","HFH.NE","HFMU.NE","HFP.NE","HFR.NE","HFU.NE","HFY.NE","HGC.NE","HGD.NE","HGGG.NE","HGM.NE","HGN.NE","HGO.NE","HGR.NE","HGRO.NE","HGU.NE","HGY.NE","HHF.NE","HHL.NE","HHS.NE","HI.NE","HID.NE","HIG.NE","HIGH.NE","HII.NE","HIKU.NE","HIP.NE","HIRE.NE","HIT.NE","HITI.NE","HIU.NE","HIVE.NE","HIX.NE","HJI.NE","HLC.NE","HLF.NE","HLPR.NE","HLS.NE","HLTH.NE","HMA.NE","HM.NE","HMC.NE","HME.NE","HMF.NE","HMI.NE","HMJI.NE","HMJR.NE","HMJU.NE","HMLO.NE","HMMJ.NE","HMP.NE","HMT.NE","HMUS.NE","HMX.NE","HNC.NE","HND.NE","HNL.NE","HNU.NE","HNY.NE","HNZ.NE","HOC.NE","HOCL.NE","HOD.NE","HODL.NE","HOG.NE","HOIL.NE","HOL.NE","HOLL.NE","HOU.NE","HP.NE","HPF.NE","HPI.NE","HPL.NE","HPQ.NE","HPR.NE","HPY.NE","HQD.NE","HQU.NE","HRA.NE","HRC.NE","HRE.NE","HRES.NE","HRH.NE","HRL.NE","HRT.NE","HRX.NE","HS.NE","HSAV.NE","HSD.NE","HSE.NE","HSH.NE","HSI.NE","HSL.NE","HSU.NE","HTA.NE","HTB.NE","HTC.NE","HTH.NE","HTL.NE","HTR.NE","HUBL.NE","HUC.NE","HUD.NE","HUF.NE","HUG.NE","HUGE.NE","HUL.NE","HULC.NE","HUN.NE","HURA.NE","HUT.NE","HUTL.NE","HUV.NE","HUZ.NE","HVG.NE","HVI.NE","HVST.NE","HVT.NE","HVU.NE","HVV.NE","HWD.NE","HWF.NE","HWO.NE","HWX.NE","HWY.NE","HXC.NE","HXCN.NE","HXD.NE","HXDM.NE","HXE.NE","HXF.NE","HXH.NE","HXI.NE","HXQ.NE","HXS.NE","HXT.NE","HXU.NE","HXX.NE","HYD.NE","HYG.NE","HYI.NE","HYPR.NE","HZ.NE","HZD.NE","HZM.NE","HZU.NE","IAE.NE","IAG.NE","IAM.NE","IAN.NE","I.NE","IB.NE","IBAT.NE","IBC.NE","IBG.NE","IBH.NE","IBIT.NE","IBR.NE","IBT.NE","ICAN.NE","IC.NE","ICAU.NE","ICC.NE","ICE.NE","ICG.NE","ICL.NE","ICM.NE","ICO.NE","ICP.NE","ICPB.NE","ID.NE","IDG.NE","IDI.NE","IDK.NE","IDL.NE","IDM.NE","IDR.NE","IDW.NE","IEI.NE","IEMB.NE","IES.NE","IFA.NE","IFC.NE","IFD.NE","IFOS.NE","IFP.NE","IFR.NE","IFRF.NE","IFX.NE","IGAF.NE","IGAM.NE","IGB.NE","IGCF.NE","IGD.NE","IGG.NE","IGLB.NE","IGM.NE","IGO.NE","IGP.NE","III.NE","IKM.NE","ILA.NE","ILC.NE","ILI.NE","ILV.NE","IMA.NE","IM.NE","IMCC.NE","IMCX.NE","IME.NE","IMEX.NE","IMG.NE","IMH.NE","IMI.NE","IMIN.NE","IML.NE","IMM.NE","IMO.NE","IMP.NE","IMR.NE","IMT.NE","IMV.NE","IMX.NE","IN.NE","INCB.NE","INDS.NE","INE.NE","INEO.NE","INLA.NE","INNO.NE","INOC.NE","INP.NE","INQ.NE","INSR.NE","INT.NE","INTL.NE","INV.NE","INX.NE","IO.NE","IOG.NE","IOM.NE","ION.NE","IONC.NE","IOT.NE","IOU.NE","IPA.NE","IP.NE","IPC.NE","IPCI.NE","IPCO.NE","IPD.NE","IPG.NE","IPL.NE","IPLP.NE","IPO.NE","IPOT.NE","IPT.NE","IQ.NE","IQD.NE","IRC.NE","IRD.NE","IRG.NE","IRI.NE","IRO.NE","IRON.NE","IRR.NE","IRV.NE","ISD.NE","ISFT.NE","ISGI.NE","ISH.NE","ISIF.NE","ISM.NE","ISO.NE","ISOL.NE","ISS.NE","ISV.NE","ITC.NE","ITG.NE","ITH.NE","ITKO.NE","ITM.NE","ITP.NE","ITR.NE","ITT.NE","ITX.NE","IVI.NE","IVN.NE","IVQ.NE","IVS.NE","IVX.NE","IXI.NE","IZ.NE","IZN.NE","IZO.NE","IZZ.NE","JADE.NE","JAEG.NE","JAG.NE","JANE.NE","JAPN.NE","J.NE","JAX.NE","JBR.NE","JCO.NE","JDF.NE","JDN.NE","JE.NE","JEC.NE","JEM.NE","JEN.NE","JET.NE","JFC.NE","JG.NE","JGW.NE","JH.NE","JJ.NE","JJJ.NE","JLR.NE","JNC.NE","JNX.NE","JOB.NE","JOR.NE","JOSE.NE","JOY.NE","JP.NE","JRI.NE","JRV.NE","JSE.NE","JSP.NE","JTC.NE","JTR.NE","JUB.NE","JUGR.NE","JUJU.NE","JUSH.NE","JWCA.NE","JWEL.NE","JZR.NE","JZZ.NE","KAL.NE","KALY.NE","KANA.NE","KAP.NE","K.NE","KAR.NE","KAS.NE","KASH.NE","KAT.NE","KBB.NE","KBEV.NE","KBG.NE","KBL.NE","KBLT.NE","KBY.NE","KC.NE","KCC.NE","KDA.NE","KDI.NE","KDK.NE","KDX.NE","KDZ.NE","KEL.NE","KEN.NE","KER.NE","KES.NE","KEW.NE","KEY.NE","KFG.NE","KFS.NE","KG.NE","KGC.NE","KGF.NE","KGI.NE","KGL.NE","KGS.NE","KHRN.NE","KIDZ.NE","KILO.NE","KING.NE","KIP.NE","KIT.NE","KIV.NE","KL.NE","KLE.NE","KLG.NE","KLM.NE","KLS.NE","KLY.NE","KMAX.NE","KMI.NE","KML.NE","KMT.NE","KNE.NE","KNG.NE","KNR.NE","KNT.NE","KNX.NE","KOPR.NE","KOR.NE","KORE.NE","KOT.NE","KPT.NE","KR.NE","KRI.NE","KRN.NE","KRS.NE","KS.NE","KSI.NE","KSK.NE","KTN.NE","KTO.NE","KTR.NE","KUB.NE","KUT.NE","KUU.NE","KWF.NE","KWG.NE","KXS.NE","KYU.NE","KZD.NE","KZZ.NE","LA.NE","LAB.NE","LABS.NE","LAC.NE","LAD.NE","LAI.NE","LAM.NE","LAN.NE","L.NE","LAT.NE","LB.NE","LBC.NE","LBI.NE","LBL.NE","LBM.NE","LBS.NE","LBY.NE","LCS.NE","LDG.NE","LDGR.NE","LDI.NE","LDS.NE","LEAD.NE","LEAF.NE","LECR.NE","LEGN.NE","LEM.NE","LEN.NE","LEO.NE","LEP.NE","LES.NE","LET.NE","LEXI.NE","LFE.NE","LFX.NE","LG.NE","LGC.NE","LGD.NE","LGN.NE","LGO.NE","LGR.NE","LHI.NE","LHR.NE","LHS.NE","LI.NE","LIB.NE","LIC.NE","LIF.NE","LIFE.NE","LIFT.NE","LIHT.NE","LILY.NE","LINK.NE","LIO.NE","LION.NE","LIQ.NE","LIR.NE","LIT.NE","LITH.NE","LITT.NE","LIVE.NE","LIX.NE","LJ.NE","LKY.NE","LL.NE","LLC.NE","LLG.NE","LLL.NE","LLP.NE","LLT.NE","LM.NE","LMC.NE","LMD.NE","LME.NE","LMG.NE","LMGC.NE","LMNL.NE","LMP.NE","LMR.NE","LMS.NE","LMT.NE","LN.NE","LNB.NE","LNC.NE","LND.NE","LNE.NE","LNF.NE","LNK.NE","LNR.NE","LOAN.NE","LOOP.NE","LOT.NE","LOTO.NE","LOVE.NE","LPC.NE","LPK.NE","LPS.NE","LRA.NE","LR.NE","LS.NE","LSC.NE","LSI.NE","LSPD.NE","LSX.NE","LTE.NE","LTV.NE","LTX.NE","LTY.NE","LUC.NE","LUFF.NE","LUG.NE","LUKY.NE","LUM.NE","LUN.NE","LUP.NE","LUX.NE","LUXX.NE","LVI.NE","LVN.NE","LVWL.NE","LXE.NE","LXG.NE","LXR.NE","LYD.NE","MAC.NE","MAD.NE","MAE.NE","MAG.NE","MAH.NE","MAI.NE","MAKA.NE","MAL.NE","MANN.NE","MAO.NE","MAP.NE","M.NE","MAR.NE","MARI.NE","MARL.NE","MAS.NE","MAT.NE","MAV.NE","MAW.NE","MAX.NE","MAXR.NE","MAY.NE","MBA.NE","MBG.NE","MBI.NE","MBN.NE","MBO.NE","MBX.NE","MCA.NE","MC.NE","MCB.NE","MCCN.NE","MCE.NE","MCF.NE","MCG.NE","MCI.NE","MCK.NE","MCL.NE","MCLC.NE","MCLD.NE","MCN.NE","MCO.NE","MCR.NE","MCS.NE","MCSB.NE","MCU.NE","MDA.NE","MD.NE","MDD.NE","MDF.NE","MDI.NE","MDL.NE","MDM.NE","MDNA.NE","MDO.NE","MDP.NE","MDV.NE","MDX.NE","ME.NE","MEC.NE","MED.NE","MEE.NE","MEG.NE","MEI.NE","MEK.NE","MENE.NE","MEQ.NE","META.NE","MET.NE","MEU.NE","MEX.NE","MEZ.NE","MFC.NE","MFI.NE","MFM.NE","MFS.NE","MFT.NE","MFX.NE","MGA.NE","MG.NE","MGB.NE","MGC.NE","MGG.NE","MGI.NE","MGM.NE","MGR.NE","MGRO.NE","MGW.NE","MGXR.NE","MHI.NE","MHYB.NE","MIC.NE","MICH.NE","MIL.NE","MILE.NE","MIMI.NE","MIN.NE","MIND.NE","MINE.NE","MINT.NE","MIO.NE","MIR.NE","MIRL.NE","MIS.NE","MIT.NE","MIVG.NE","MIX.NE","MJ.NE","MJAR.NE","MJJ.NE","MJN.NE","MJRX.NE","MJS.NE","MJX.NE","MKA.NE","MKB.NE","MKC.NE","MKNA.NE","MKO.NE","MKP.NE","MKR.NE","MKT.NE","ML.NE","MLG.NE","MLK.NE","MLM.NE","MLN.NE","MLR.NE","MLY.NE","MMA.NE","MM.NE","MMAT.NE","MMC.NE","MMED.NE","MMEN.NE","MMG.NE","MMI.NE","MMJ.NE","MMM.NE","MMN.NE","MMO.NE","MMS.NE","MMV.NE","MMX.NE","MMY.NE","MN.NE","MNC.NE","MND.NE","MNG.NE","MNO.NE","MNR.NE","MNS.NE","MNT.NE","MNW.NE","MNX.NE","MNY.NE","MOG.NE","MOGO.NE","MOI.NE","MOL.NE","MON.NE","MONT.NE","MOO.NE","MOON.NE","MOS.NE","MOTA.NE","MOV.NE","MOX.NE","MOZ.NE","MPC.NE","MPCF.NE","MPH.NE","MPLS.NE","MPT.NE","MPV.NE","MPVD.NE","MPX.NE","MPXI.NE","MQR.NE","MQX.NE","MRA.NE","MRB.NE","MRBL.NE","MRC.NE","MRD.NE","MRE.NE","MREY.NE","MRI.NE","MRL.NE","MRM.NE","MRO.NE","MRS.NE","MRU.NE","MRZ.NE","MSA.NE","MSI.NE","MSL.NE","MSP.NE","MSR.NE","MSV.NE","MTA.NE","MT.NE","MTB.NE","MTC.NE","MTEC.NE","MTH.NE","MTL.NE","MTLO.NE","MTO.NE","MTRX.NE","MTS.NE","MTT.NE","MTU.NE","MTX.NE","MTY.NE","MTZ.NE","MUB.NE","MULC.NE","MUM.NE","MUMC.NE","MUN.NE","MUR.NE","MUS.NE","MVAI.NE","MV.NE","MVI.NE","MVM.NE","MVMD.NE","MVN.NE","MVP.NE","MVT.NE","MVY.NE","MWD.NE","MWM.NE","MWMN.NE","MWX.NE","MX.NE","MXF.NE","MXG.NE","MXL.NE","MXM.NE","MXR.NE","MXU.NE","MXX.NE","MYA.NE","MY.NE","MYID.NE","MYM.NE","MYR.NE","MZI.NE","NA.NE","NAB.NE","NAC.NE","NAL.NE","NALT.NE","NAM.NE","NAN.NE","NAP.NE","N.NE","NAR.NE","NB.NE","NBR.NE","NBU.NE","NBUD.NE","NBVA.NE","NBY.NE","NBZ.NE","NCA.NE","NC.NE","NCE.NE","NCF.NE","NCI.NE","NCP.NE","NCU.NE","NCX.NE","NDM.NE","NDQ.NE","NDR.NE","NDVA.NE","NED.NE","NEE.NE","NEM.NE","NEO.NE","NEPT.NE","NER.NE","NERD.NE","NET.NE","NETC.NE","NEV.NE","NEWU.NE","NEXA.NE","NEXT.NE","NFAM.NE","NF.NE","NFI.NE","NG.NE","NGC.NE","NGD.NE","NGE.NE","NGEN.NE","NGEX.NE","NGPE.NE","NGQ.NE","NGW.NE","NGY.NE","NGZ.NE","NHC.NE","NHK.NE","NHP.NE","NHS.NE","NHYB.NE","NIA.NE","NI.NE","NIC.NE","NII.NE","NIK.NE","NIKL.NE","NIM.NE","NIO.NE","NIP.NE","NIR.NE","NITE.NE","NKL.NE","NKO.NE","NKW.NE","NL.NE","NLB.NE","NLC.NE","NLH.NE","NLR.NE","NLTA.NE","NLV.NE","NM.NE","NMD.NE","NMG.NE","NMI.NE","NML.NE","NMX.NE","NNA.NE","NNN.NE","NNO.NE","NNP.NE","NNX.NE","NOA.NE","NOB.NE","NOG.NE","NOM.NE","NORA.NE","NOR.NE","NOT.NE","NOU.NE","NOV.NE","NPA.NE","NP.NE","NPC.NE","NPD.NE","NPI.NE","NPK.NE","NPRF.NE","NPS.NE","NPT.NE","NQE.NE","NR.NE","NREA.NE","NRE.NE","NRG.NE","NRI.NE","NRM.NE","NRN.NE","NRTH.NE","NSAU.NE","NSCB.NE","NSCE.NE","NSCI.NE","NSE.NE","NSG.NE","NSGE.NE","NSHS.NE","NSM.NE","NSP.NE","NST.NE","NSU.NE","NSX.NE","NTAR.NE","NTB.NE","NTC.NE","NTE.NE","NTM.NE","NTQ.NE","NTR.NE","NTRL.NE","NTS.NE","NTW.NE","NTY.NE","NUAG.NE","NU.NE","NUBF.NE","NUG.NE","NUGT.NE","NUK.NE","NUMI.NE","NUR.NE","NUS.NE","NUU.NE","NUX.NE","NVA.NE","NVC.NE","NVCN.NE","NVG.NE","NVH.NE","NVI.NE","NVLN.NE","NVM.NE","NVO.NE","NVS.NE","NVT.NE","NVV.NE","NVX.NE","NVY.NE","NW.NE","NWC.NE","NWE.NE","NWES.NE","NWI.NE","NWX.NE","NX.NE","NXC.NE","NXE.NE","NXF.NE","NXG.NE","NXJ.NE","NXN.NE","NXO.NE","NXS.NE","NXT.NE","NXU.NE","NYX.NE","NZ.NE","NZC.NE","NZN.NE","NZP.NE","NZZ.NE","OAA.NE","OBE.NE","OCG.NE","OCN.NE","OCO.NE","OCX.NE","OEC.NE","OEE.NE","OEG.NE","OG.NE","OGC.NE","OGD.NE","OGI.NE","OGO.NE","OH.NE","OIC.NE","OIII.NE","OIL.NE","OILS.NE","OJO.NE","OK.NE","OLA.NE","OLV.NE","OLY.NE","OM.NE","OMI.NE","OML.NE","OMM.NE","OMNI.NE","ON.NE","ONC.NE","ONE.NE","ONEB.NE","ONEQ.NE","ONEX.NE","ONG.NE","ONV.NE","OOI.NE","OOO.NE","OOR.NE","OPC.NE","OPI.NE","OPS.NE","OPT.NE","OPV.NE","OPW.NE","ORA.NE","OR.NE","ORCD.NE","ORE.NE","ORG.NE","ORGN.NE","ORI.NE","ORL.NE","ORM.NE","ORO.NE","ORR.NE","ORS.NE","ORT.NE","ORTH.NE","ORV.NE","ORX.NE","OS.NE","OSB.NE","OSH.NE","OSI.NE","OSK.NE","OSO.NE","OSP.NE","OSS.NE","OSU.NE","OTC.NE","OTEX.NE","OTSO.NE","OVAT.NE","OWLI.NE","OX.NE","OXC.NE","OXF.NE","OY.NE","OYL.NE","PAA.NE","PA.NE","PAAS.NE","PAC.NE","PAD.NE","PAID.NE","PAL.NE","PAM.NE","PAN.NE","P.NE","PAS.NE","PAT.NE","PAU.NE","PAY.NE","PBB.NE","PBD.NE","PBH.NE","PBI.NE","PBIT.NE","PBL.NE","PBM.NE","PBR.NE","PBS.NE","PBX.NE","PCF.NE","PCI.NE","PCLO.NE","PCO.NE","PCON.NE","PCOR.NE","PCQ.NE","PCR.NE","PCY.NE","PD.NE","PDC.NE","PDF.NE","PDH.NE","PDIV.NE","PDL.NE","PDM.NE","PDN.NE","PDO.NE","PDP.NE","PDQ.NE","PDTI.NE","PDV.NE","PEA.NE","PE.NE","PEEK.NE","PEH.NE","PEI.NE","PEMC.NE","PEN.NE","PEO.NE","PER.NE","PERK.NE","PERU.NE","PET.NE","PEU.NE","PEX.NE","PEY.NE","PFAE.NE","PFB.NE","PFC.NE","PFG.NE","PFH.NE","PFIA.NE","PFL.NE","PFM.NE","PFMN.NE","PFMS.NE","PG.NE","PGB.NE","PGC.NE","PGD.NE","PGDC.NE","PGE.NE","PGF.NE","PGG.NE","PGK.NE","PGL.NE","PGM.NE","PGP.NE","PGS.NE","PGV.NE","PGX.NE","PGZ.NE","PHA.NE","PHD.NE","PHE.NE","PHGI.NE","PHI.NE","PHM.NE","PHO.NE","PHR.NE","PHW.NE","PHX.NE","PHYS.NE","PIB.NE","PID.NE","PIF.NE","PIH.NE","PIKE.NE","PILL.NE","PIN.NE","PINC.NE","PINV.NE","PIPE.NE","PIVT.NE","PJO.NE","PJT.NE","PJX.NE","PKG.NE","PKI.NE","PKK.NE","PKT.NE","PLA.NE","PLAN.NE","PL.NE","PLAY.NE","PLC.NE","PLDI.NE","PLG.NE","PLI.NE","PLS.NE","PLTH.NE","PLU.NE","PLUS.NE","PLV.NE","PLX.NE","PLY.NE","PMA.NE","PMC.NE","PME.NE","PMED.NE","PMI.NE","PMIF.NE","PML.NE","PMM.NE","PMN.NE","PMNT.NE","PMR.NE","PMT.NE","PMTS.NE","PMX.NE","PNE.NE","PNG.NE","PNN.NE","PNO.NE","PNP.NE","POC.NE","POE.NE","POG.NE","POI.NE","POM.NE","POND.NE","PONY.NE","POOL.NE","POR.NE","PORE.NE","POT.NE","POU.NE","POW.NE","PP.NE","PPE.NE","PPK.NE","PPL.NE","PPM.NE","PPP.NE","PPR.NE","PPS.NE","PPX.NE","PPY.NE","PQE.NE","PRA.NE","PR.NE","PRB.NE","PRD.NE","PRE.NE","PREF.NE","PREV.NE","PRG.NE","PRH.NE","PRI.NE","PRIM.NE","PRK.NE","PRL.NE","PRM.NE","PRMO.NE","PRMW.NE","PRN.NE","PRO.NE","PROS.NE","PRP.NE","PRQ.NE","PRS.NE","PRT.NE","PRU.NE","PRW.NE","PRYM.NE","PRZ.NE","PSA.NE","PSB.NE","PSD.NE","PSE.NE","PSH.NE","PSI.NE","PSK.NE","PSL.NE","PSLV.NE","PST.NE","PSV.NE","PSY.NE","PT.NE","PTB.NE","PTC.NE","PTE.NE","PTF.NE","PTG.NE","PTK.NE","PTM.NE","PTP.NE","PTQ.NE","PTR.NE","PTS.NE","PTU.NE","PTV.NE","PTX.NE","PUC.NE","PUD.NE","PUF.NE","PUL.NE","PUMA.NE","PUM.NE","PUMP.NE","PUR.NE","PURE.NE","PUSH.NE","PVG.NE","PVOT.NE","PWF.NE","PWM.NE","PWR.NE","PWT.NE","PXA.NE","PX.NE","PXC.NE","PXG.NE","PXI.NE","PXS.NE","PXT.NE","PXU.NE","PXX.NE","PYD.NE","PYF.NE","PYR.NE","PYT.NE","PZA.NE","PZC.NE","PZW.NE","QAH.NE","QBA.NE","QBB.NE","QBOT.NE","QBTL.NE","QCA.NE","QCB.NE","QCC.NE","QCD.NE","QCE.NE","QCH.NE","QCN.NE","QCP.NE","QDX.NE","QDXH.NE","QEBH.NE","QEBL.NE","QEC.NE","QEF.NE","QEM.NE","QF.NE","QGB.NE","QGE.NE","QGL.NE","QGR.NE","QHY.NE","QI.NE","QIC.NE","QIE.NE","QIF.NE","QIS.NE","QIT.NE","QMA.NE","QMC.NE","QMG.NE","QMI.NE","QMV.NE","QMX.NE","QMY.NE","QNC.NE","QPT.NE","QQ.NE","QQC.NE","QRC.NE","QRD.NE","QRH.NE","QRI.NE","QRM.NE","QRO.NE","QRT.NE","QSB.NE","QSC.NE","QSR.NE","QST.NE","QTA.NE","QTIP.NE","QTRH.NE","QUDV.NE","QUIG.NE","QUIS.NE","QUS.NE","QUU.NE","QX.NE","QXM.NE","QXP.NE","QYOU.NE","QZM.NE","RAB.NE","RAD.NE","RAK.NE","R.NE","RAYN.NE","RBA.NE","RB.NE","RBDI.NE","RBE.NE","RBNK.NE","RBO.NE","RBOT.NE","RBS.NE","RBX.NE","RCAN.NE","RC.NE","RCD.NE","RCDB.NE","RCE.NE","RCG.NE","RCH.NE","RCK.NE","RCLF.NE","RCSB.NE","RCT.NE","RCU.NE","RCUB.NE","RD.NE","RDE.NE","RDI.NE","RDK.NE","RDKO.NE","RDL.NE","RDS.NE","RDU.NE","REAL.NE","RE.NE","REC.NE","RECO.NE","RECP.NE","REEM.NE","REG.NE","REIT.NE","REK.NE","REKO.NE","RELA.NE","REL.NE","REM.NE","REN.NE","REO.NE","RER.NE","RET.NE","REVO.NE","REW.NE","REX.NE","REZ.NE","RFC.NE","RFR.NE","RG.NE","RGC.NE","RGD.NE","RGGB.NE","RGI.NE","RGLD.NE","RGM.NE","RGO.NE","RGRE.NE","RGT.NE","RGX.NE","RGZ.NE","RHA.NE","RHC.NE","RHT.NE","RIC.NE","RICH.NE","RID.NE","RIDH.NE","RIE.NE","RIEH.NE","RIG.NE","RIIN.NE","RIN.NE","RINT.NE","RIO.NE","RIRA.NE","RIR.NE","RIT.NE","RIV.NE","RIW.NE","RJ.NE","RK.NE","RKN.NE","RKR.NE","RKS.NE","RLB.NE","RLD.NE","RLDR.NE","RLE.NE","RLG.NE","RLP.NE","RLSC.NE","RLTY.NE","RLV.NE","RLY.NE","RM.NE","RMBO.NE","RMD.NE","RME.NE","RMI.NE","RMK.NE","RML.NE","RMMI.NE","RMN.NE","RMO.NE","RMP.NE","RMX.NE","RN.NE","RNG.NE","RNP.NE","RNR.NE","RNW.NE","RNX.NE","ROB.NE","ROBO.NE","ROC.NE","ROCK.NE","ROE.NE","ROG.NE","ROI.NE","ROK.NE","ROMJ.NE","ROOT.NE","ROS.NE","ROT.NE","ROVR.NE","ROX.NE","ROXG.NE","ROYL.NE","RP.NE","RPC.NE","RPD.NE","RPDH.NE","RPF.NE","RPM.NE","RPN.NE","RPP.NE","RPR.NE","RPS.NE","RPSB.NE","RPU.NE","RPX.NE","RQB.NE","RQE.NE","RQF.NE","RQG.NE","RQH.NE","RQI.NE","RQJ.NE","RQK.NE","RQL.NE","RQN.NE","RRI.NE","RRK.NE","RRL.NE","RRS.NE","RRX.NE","RSI.NE","RSL.NE","RSS.NE","RST.NE","RSV.NE","RSY.NE","RTA.NE","RTG.NE","RTH.NE","RTI.NE","RTM.NE","RTN.NE","RUBH.NE","RUBY.NE","RUD.NE","RUDH.NE","RUE.NE","RUEH.NE","RUG.NE","RUM.NE","RUN.NE","RUP.NE","RUSA.NE","RUS.NE","RUSB.NE","RVG.NE","RVL.NE","RVR.NE","RVV.NE","RVX.NE","RW.NE","RWC.NE","RWE.NE","RWR.NE","RWU.NE","RWW.NE","RWX.NE","RX.NE","RXD.NE","RXE.NE","RXM.NE","RY.NE","RYE.NE","RYO.NE","RYR.NE","RYU.NE","RZE.NE","RZL.NE","RZR.NE","RZX.NE","RZZ.NE","SA.NE","SAAS.NE","SAE.NE","SAFE.NE","SAH.NE","SAI.NE","SAIS.NE","SAM.NE","SAN.NE","SAO.NE","SAP.NE","S.NE","SAT.NE","SAU.NE","SAY.NE","SB.NE","SBB.NE","SBC.NE","SBD.NE","SBI.NE","SBM.NE","SBN.NE","SBND.NE","SBR.NE","SBT.NE","SBW.NE","SBX.NE","SCA.NE","SCAD.NE","SCAN.NE","SCB.NE","SCC.NE","SCD.NE","SCG.NE","SCL.NE","SCLT.NE","SCOT.NE","SCR.NE","SCT.NE","SCU.NE","SCV.NE","SCY.NE","SCYB.NE","SCZ.NE","SDC.NE","SDE.NE","SDI.NE","SDL.NE","SDR.NE","SDS.NE","SDX.NE","SDY.NE","SEA.NE","SEB.NE","SEC.NE","SEED.NE","SEI.NE","SEK.NE","SEN.NE","SENS.NE","SES.NE","SEV.NE","SFD.NE","SFI.NE","SFIX.NE","SFM.NE","SFR.NE","SFT.NE","SFX.NE","SGA.NE","SG.NE","SGB.NE","SGC.NE","SGE.NE","SGF.NE","SGH.NE","SGI.NE","SGMA.NE","SGM.NE","SGN.NE","SGQ.NE","SGRO.NE","SGU.NE","SGW.NE","SGX.NE","SGY.NE","SGZ.NE","SHA.NE","SH.NE","SHC.NE","SHE.NE","SHJ.NE","SHL.NE","SHLE.NE","SHM.NE","SHOP.NE","SHP.NE","SHRC.NE","SHRM.NE","SHU.NE","SHV.NE","SHZ.NE","SIA.NE","SIC.NE","SID.NE","SIE.NE","SIG.NE","SII.NE","SIL.NE","SIM.NE","SINT.NE","SIQ.NE","SIR.NE","SIRE.NE","SIS.NE","SIX.NE","SIXW.NE","SJ.NE","SJL.NE","SKE.NE","SKK.NE","SKP.NE","SKRB.NE","SKRR.NE","SKYG.NE","SKYL.NE","SLAU.NE","SLF.NE","SLG.NE","SLK.NE","SLL.NE","SLNG.NE","SLT.NE","SLVR.NE","SLW.NE","SLZ.NE","SMB.NE","SMC.NE","SMD.NE","SME.NE","SMF.NE","SMI.NE","SML.NE","SMM.NE","SMN.NE","SMO.NE","SMP.NE","SMR.NE","SMS.NE","SMT.NE","SMY.NE","SNA.NE","SN.NE","SNC.NE","SNF.NE","SNG.NE","SNL.NE","SNM.NE","SNN.NE","SNP.NE","SNR.NE","SNS.NE","SNV.NE","SO.NE","SOC.NE","SOCK.NE","SOF.NE","SOFT.NE","SOG.NE","SOI.NE","SOIL.NE","SOJ.NE","SOL.NE","SOLG.NE","SOLO.NE","SOLR.NE","SOMA.NE","SONA.NE","SOP.NE","SOR.NE","SOU.NE","SOW.NE","SOX.NE","SOY.NE","SPA.NE","SP.NE","SPB.NE","SPD.NE","SPDR.NE","SPE.NE","SPEY.NE","SPF.NE","SPFY.NE","SPG.NE","SPI.NE","SPK.NE","SPMT.NE","SPN.NE","SPO.NE","SPOR.NE","SPOT.NE","SPP.NE","SPPP.NE","SPR.NE","SPT.NE","SPX.NE","SQA.NE","SQD.NE","SQG.NE","SQID.NE","SQP.NE","SQR.NE","SQX.NE","SRA.NE","SR.NE","SRC.NE","SRCH.NE","SRE.NE","SRES.NE","SRG.NE","SRHI.NE","SRI.NE","SRL.NE","SRS.NE","SRX.NE","SSA.NE","SS.NE","SSC.NE","SSE.NE","SSG.NE","SSL.NE","SSN.NE","SSO.NE","SSP.NE","SSRM.NE","SSS.NE","SSV.NE","SSVR.NE","STA.NE","ST.NE","STAR.NE","STB.NE","STC.NE","STE.NE","STEP.NE","STGO.NE","STH.NE","STI.NE","STIL.NE","STLC.NE","STM.NE","STMP.NE","STN.NE","STND.NE","STPL.NE","STS.NE","STT.NE","STU.NE","STV.NE","STX.NE","SU.NE","SUGR.NE","SUM.NE","SUN.NE","SUNM.NE","SUP.NE","SUR.NE","SURG.NE","SUSA.NE","SUV.NE","SVA.NE","SV.NE","SVC.NE","SVE.NE","SVG.NE","SVI.NE","SVM.NE","SVO.NE","SVR.NE","SVS.NE","SVT.NE","SVY.NE","SWA.NE","SW.NE","SWH.NE","SWIS.NE","SWP.NE","SWY.NE","SX.NE","SXE.NE","SXI.NE","SXL.NE","SXP.NE","SXR.NE","SXTY.NE","SXX.NE","SY.NE","SYD.NE","SYH.NE","SYI.NE","SYLD.NE","SYN.NE","SYZ.NE","SZLS.NE","SZM.NE","TAAL.NE","TA.NE","TAC.NE","TAE.NE","TAI.NE","TAJ.NE","TAK.NE","TAL.NE","TAN.NE","TAO.NE","T.NE","TAR.NE","TAU.NE","TBIX.NE","TBL.NE","TBP.NE","TBRD.NE","TBX.NE","TCA.NE","TCAN.NE","TCC.NE","TCI.NE","TCLB.NE","TCN.NE","TCO.NE","TCS.NE","TCSB.NE","TCW.NE","TD.NE","TDB.NE","TDG.NE","TE.NE","TEC.NE","TECT.NE","TEI.NE","TEK.NE","TEL.NE","TELO.NE","TEM.NE","TEMP.NE","TEN.NE","TENO.NE","TEQ.NE","TER.NE","TES.NE","TEST.NE","TET.NE","TETH.NE","TEV.NE","TF.NE","TFC.NE","TFII.NE","TFS.NE","TG.NE","TGED.NE","TGFI.NE","TGH.NE","TGIF.NE","TGK.NE","TGL.NE","TGO.NE","TGOD.NE","TGR.NE","TGRE.NE","TGV.NE","TGX.NE","TGZ.NE","TH.NE","THC.NE","THE.NE","THH.NE","THNK.NE","THO.NE","THP.NE","THRM.NE","THU.NE","THX.NE","TI.NE","TIC.NE","TIG.NE","TIH.NE","TII.NE","TIL.NE","TILT.NE","TILV.NE","TIM.NE","TIME.NE","TIN.NE","TK.NE","TKO.NE","TKR.NE","TKS.NE","TKU.NE","TKX.NE","TLA.NE","TLF.NE","TLG.NE","TLK.NE","TLO.NE","TLT.NE","TLV.NE","TM.NE","TMAS.NE","TMB.NE","TMD.NE","TMED.NE","TMG.NE","TMI.NE","TML.NE","TMM.NE","TMQ.NE","TMR.NE","TMS.NE","TNA.NE","TN.NE","TNC.NE","TNP.NE","TNR.NE","TNX.NE","TNY.NE","TO.NE","TOC.NE","TOE.NE","TOG.NE","TOKI.NE","TOM.NE","TON.NE","TOOL.NE","TORQ.NE","TORR.NE","TOS.NE","TOT.NE","TOU.NE","TOY.NE","TPAY.NE","TPC.NE","TPE.NE","TPH.NE","TPK.NE","TPL.NE","TPRF.NE","TPS.NE","TPU.NE","TQCD.NE","TQGD.NE","TQGM.NE","TQSM.NE","TRA.NE","TRAD.NE","TRAK.NE","TR.NE","TRB.NE","TRC.NE","TRCE.NE","TREK.NE","TRG.NE","TRI.NE","TRIL.NE","TRL.NE","TRM.NE","TRO.NE","TRP.NE","TRQ.NE","TRS.NE","TRST.NE","TRU.NE","TRUL.NE","TRZ.NE","TSD.NE","TSG.NE","TSGI.NE","TSI.NE","TSK.NE","TSL.NE","TSN.NE","TSP.NE","TSU.NE","TTC.NE","TTD.NE","TTM.NE","TTP.NE","TTR.NE","TTS.NE","TTT.NE","TTX.NE","TTZ.NE","TUD.NE","TUF.NE","TUHY.NE","TULB.NE","TUO.NE","TUSB.NE","TUSK.NE","TV.NE","TVE.NE","TVI.NE","TVK.NE","TVL.NE","TVR.NE","TWC.NE","TWM.NE","TWR.NE","TWX.NE","TWY.NE","TXF.NE","TXG.NE","TXP.NE","TXR.NE","TXX.NE","TYE.NE","TYMB.NE","TYP.NE","TZM.NE","TZR.NE","TZS.NE","TZZ.NE","U.NE","UAV.NE","UBM.NE","UBN.NE","UBQ.NE","UCL.NE","UCU.NE","UDA.NE","UEX.NE","UFC.NE","UG.NE","UGD.NE","UGE.NE","UGM.NE","UHD.NE","UHO.NE","UI.NE","ULI.NE","ULT.NE","ULV.NE","UMI.NE","UN.NE","UNC.NE","UNI.NE","UNO.NE","UNS.NE","UNV.NE","UP.NE","UPCO.NE","UR.NE","URB.NE","URC.NE","URE.NE","URG.NE","URL.NE","URZ.NE","USA.NE","US.NE","USB.NE","USCO.NE","USGD.NE","USHA.NE","USO.NE","USS.NE","UTL.NE","UTY.NE","UVN.NE","UWE.NE","UXM.NE","VA.NE","VAB.NE","VAH.NE","VAL.NE","VAN.NE","VANC.NE","VAPN.NE","VAU.NE","VAX.NE","VBAL.NE","VB.NE","VBG.NE","VBU.NE","VBV.NE","VCAN.NE","VC.NE","VCB.NE","VCE.NE","VCI.NE","VCIP.NE","VCM.NE","VCN.NE","VCNS.NE","VCOM.NE","VCT.NE","VCV.NE","VDO.NE","VDU.NE","VDY.NE","VE.NE","VEC.NE","VEE.NE","VEF.NE","VEH.NE","VEIN.NE","VEL.NE","VENI.NE","VENZ.NE","VEQT.NE","VERT.NE","VET.NE","VEXT.NE","VFF.NE","VFV.NE","VGAB.NE","VG.NE","VGCX.NE","VGD.NE","VGG.NE","VGH.NE","VGL.NE","VGLD.NE","VGM.NE","VGN.NE","VGO.NE","VGRO.NE","VGV.NE","VGW.NE","VGZ.NE","VHI.NE","VI.NE","VIBE.NE","VIDA.NE","VIDY.NE","VII.NE","VIN.NE","VIO.NE","VIPR.NE","VIR.NE","VIS.NE","VIT.NE","VIU.NE","VIV.NE","VIVO.NE","VLA.NE","VLB.NE","VLC.NE","VLE.NE","VLI.NE","VLN.NE","VLNS.NE","VLQ.NE","VLT.NE","VLV.NE","VM.NE","VMD.NE","VML.NE","VMO.NE","VMX.NE","VNP.NE","VNR.NE","VO.NE","VOLT.NE","VONE.NE","VOTI.NE","VOX.NE","VP.NE","VPH.NE","VPI.NE","VPN.NE","VPT.NE","VPY.NE","VQA.NE","VQS.NE","VR.NE","VRB.NE","VRD.NE","VRE.NE","VREO.NE","VRO.NE","VRR.NE","VRS.NE","VRT.NE","VRX.NE","VRY.NE","VS.NE","VSB.NE","VSBY.NE","VSC.NE","VSG.NE","VSN.NE","VSP.NE","VSR.NE","VST.NE","VTI.NE","VTT.NE","VTX.NE","VUI.NE","VUL.NE","VUN.NE","VUS.NE","VUX.NE","VV.NE","VVC.NE","VVL.NE","VVO.NE","VVV.NE","VXC.NE","VXL.NE","VXM.NE","VXS.NE","VYC.NE","VYGR.NE","VZLA.NE","VZZ.NE","WA.NE","WAB.NE","WAF.NE","WAL.NE","WAR.NE","WATR.NE","WAV.NE","WAVE.NE","WAYL.NE","WBE.NE","WBIO.NE","WBR.NE","WCB.NE","WCC.NE","WCE.NE","WCN.NE","WCP.NE","WDG.NE","WDO.NE","WEB.NE","WED.NE","WEE.NE","WEED.NE","WEF.NE","WEI.NE","WEII.NE","WELL.NE","WEQ.NE","WFC.NE","WFG.NE","WFS.NE","WFT.NE","WG.NE","WGC.NE","WGF.NE","WGO.NE","WHM.NE","WHN.NE","WHY.NE","WI.NE","WIFI.NE","WIL.NE","WILD.NE","WIN.NE","WINS.NE","WJA.NE","WJX.NE","WKG.NE","WKM.NE","WL.NE","WLF.NE","WLLW.NE","WLV.NE","WM.NE","WMD.NE","WMK.NE","WML.NE","WMR.NE","WMT.NE","WN.NE","WNDR.NE","WOMN.NE","WOW.NE","WP.NE","WPK.NE","WPM.NE","WPN.NE","WPQ.NE","WPRT.NE","WPT.NE","WPX.NE","WR.NE","WRG.NE","WRI.NE","WRLD.NE","WRN.NE","WRP.NE","WRR.NE","WRW.NE","WRX.NE","WRY.NE","WSE.NE","WSK.NE","WSM.NE","WSP.NE","WST.NE","WTE.NE","WTR.NE","WUC.NE","WWT.NE","WXM.NE","WZR.NE","XAG.NE","XAL.NE","XAM.NE","X.NE","XAU.NE","XAW.NE","XBAL.NE","XBB.NE","XBC.NE","XBLK.NE","XBM.NE","XBZ.NE","XCB.NE","XCD.NE","XCG.NE","XCH.NE","XCNS.NE","XCR.NE","XCS.NE","XCSR.NE","XCT.NE","XCV.NE","XCX.NE","XDC.NE","XDG.NE","XDGH.NE","XDIV.NE","XDSR.NE","XDU.NE","XDUH.NE","XDV.NE","XEB.NE","XEC.NE","XEF.NE","XEG.NE","XEH.NE","XEI.NE","XEM.NE","XEN.NE","XEQT.NE","XESG.NE","XEU.NE","XFA.NE","XFC.NE","XFF.NE","XFH.NE","XFI.NE","XFN.NE","XFR.NE","XFS.NE","XGB.NE","XGC.NE","XGD.NE","XGGB.NE","XGI.NE","XGR.NE","XGRO.NE","XHB.NE","XHC.NE","XHD.NE","XHU.NE","XHY.NE","XIA.NE","XI.NE","XIC.NE","XID.NE","XIG.NE","XIM.NE","XIN.NE","XINC.NE","XIT.NE","XIU.NE","XL.NE","XLB.NE","XLY.NE","XMA.NE","XMC.NE","XMD.NE","XME.NE","XMG.NE","XMH.NE","XMI.NE","XML.NE","XMM.NE","XMS.NE","XMTM.NE","XMU.NE","XMV.NE","XMW.NE","XMY.NE","XND.NE","XOP.NE","XPF.NE","XPHY.NE","XQB.NE","XQLT.NE","XQQ.NE","XR.NE","XRAY.NE","XRB.NE","XRC.NE","XRE.NE","XRO.NE","XRTX.NE","XRX.NE","XSAB.NE","XS.NE","XSB.NE","XSC.NE","XSEA.NE","XSE.NE","XSEM.NE","XSH.NE","XSI.NE","XSMC.NE","XSMH.NE","XSP.NE","XSQ.NE","XSR.NE","XST.NE","XSTB.NE","XSU.NE","XSUS.NE","XTC.NE","XTD.NE","XTG.NE","XTHC.NE","XTM.NE","XTR.NE","XTRX.NE","XTT.NE","XUH.NE","XUS.NE","XUSR.NE","XUT.NE","XUU.NE","XVLU.NE","XWD.NE","XX.NE","XXM.NE","XYL.NE","YAK.NE","Y.NE","YCM.NE","YD.NE","YDX.NE","YES.NE","YFI.NE","YGR.NE","YGT.NE","YMI.NE","YNV.NE","YOO.NE","YRB.NE","YRI.NE","YSS.NE","YT.NE","YTY.NE","YVI.NE","YXM.NE","ZAD.NE","ZAG.NE","ZAIR.NE","Z.NE","ZAR.NE","ZAZ.NE","ZBAL.NE","ZBBB.NE","ZBK.NE","ZBR.NE","ZC.NE","ZCB.NE","ZCC.NE","ZCH.NE","ZCL.NE","ZCM.NE","ZCN.NE","ZCON.NE","ZCPB.NE","ZCS.NE","ZDB.NE","ZDC.NE","ZDH.NE","ZDI.NE","ZDJ.NE","ZDM.NE","ZDV.NE","ZDY.NE","ZEA.NE","ZEB.NE","ZEE.NE","ZEF.NE","ZEM.NE","ZENA.NE","ZEN.NE","ZEO.NE","ZEQ.NE","ZESG.NE","ZEU.NE","ZEUS.NE","ZFC.NE","ZFH.NE","ZFL.NE","ZFM.NE","ZFN.NE","ZFR.NE","ZFS.NE","ZGB.NE","ZGD.NE","ZGI.NE","ZGQ.NE","ZGRO.NE","ZGSB.NE","ZHP.NE","ZHU.NE","ZHY.NE","ZIC.NE","ZID.NE","ZIN.NE","ZINC.NE","ZJG.NE","ZJK.NE","ZJN.NE","ZJO.NE","ZKL.NE","ZLB.NE","ZLC.NE","ZLD.NE","ZLE.NE","ZLH.NE","ZLI.NE","ZLU.NE","ZMA.NE","ZMBS.NE","ZMD.NE","ZMI.NE","ZMID.NE","ZMP.NE","ZMS.NE","ZMSB.NE","ZMT.NE","ZMU.NE","ZNG.NE","ZNK.NE","ZNQ.NE","ZNX.NE","ZOM.NE","ZOMD.NE","ZON.NE","ZONE.NE","ZPAY.NE","ZPH.NE","ZPL.NE","ZPR.NE","ZPS.NE","ZPW.NE","ZQB.NE","ZQQ.NE","ZRE.NE","ZRI.NE","ZRO.NE","ZRR.NE","ZSB.NE","ZSML.NE","ZSP.NE","ZST.NE","ZSU.NE","ZTE.NE","ZTL.NE","ZTM.NE","ZTR.NE","ZTS.NE","ZUB.NE","ZUD.NE","ZUE.NE","ZUH.NE","ZUM.NE","ZUP.NE","ZUQ.NE","ZUT.NE","ZVC.NE","ZVI.NE","ZVU.NE","ZWA.NE","ZWB.NE","ZWC.NE","ZWE.NE","ZWG.NE","ZWH.NE","ZWK.NE","ZWP.NE","ZWS.NE","ZWU.NE","ZX.NE","ZXM.NE","ZYME.NE","ZZZ.NE","ZZZD.NE"]
    # tickers = ["AACG","ABB","ABEV","ACH","ADAP","ADXN","AEG","AIH","AKO-A","AKO-B","AKTX","AMBO","AMOV","AMRN","AMX","ANCN","ANPC","ANTE","APOP","ARGX","ASLN","ASML","ASND","ASR","ASX","ATHE","ATHM","ATV","AU","AUTL","AVAL","AVDL","AZN","AZUL","BABA","BAK","BBAR","BBD","BBDO","BBL","BBVA","BCH","BCS","BCYC","BEDU","BEST","BFRA","BGNE","BHP","BIDU","BILI","BITA","BLRX","BMA","BNTX","BP","BRFS","BSAC","BSBR","BSMX","BTI","BUD","BVN","BVXV","BWAY","BZUN","CAJ","CAN","CANF","CANG","CBD","CCM","CCU","CEA","CEO","CEPU","CHA","CHL","CHT","CHU","CIB","CIG","CIG-C","CIH","CLGN","CLLS","CMCM","CNF","COE","CPAC","CRESY","CRH","CRTO","CS","CTK","CUK","CX","CYAD","DAO","DAVA","DBVT","DEO","DL","DNK","DOYU","DQ","DRD","DUO","DXF","E","EBR","EBR-B","EC","EDAP","EDN","EDU","EH","ELP","ENIA","ENIC","EQNR","ERIC","ERJ","ERYP","FANH","FEDU","FENG","FINV","FLY","FMS","FMX","FORTY","FRSX","FUTU","FWP","GDS","GENE","GFI","GGAL","GGB","GHG","GLPG","GMAB","GNFT","GOL","GRAM","GRFS","GRVY","GSH","GSK","GSUM","GSX","GWPH","HCM","HDB","HHR","HIMX","HKIB","HLG","HMC","HMI","HMY","HNP","HSBC","HTHT","HUIZ","HUYA","HX","IBA","IBN","ICLK","IHG","IMAB","IMMP","IMOS","IMRN","INFY","ING","IPHA","IQ","IRCP","IRS","ITCB","ITMR","ITUB","IX","JD","JFIN","JFU","JG","JHX","JKS","JMIA","JOBS","JP","JRJC","JT","KB","KC","KEP","KOF","KRKR","KT","KTOV","KZIA","LAIX","LEJU","LFC","LINX","LITB","LIZI","LN","LND","LOMA","LOV","LPL","LTM","LX","LYG","MBT","MDGS","MESO","MFG","MFGP","MFH","MITO","MIXT","MKD","MLCO","MOGU","MOHO","MOMO","MOR","MREO","MSC","MT","MTL","MTL-P","MTLS","MTP","MUFG","NCNA","NCTY","NEW","NGG","NICE","NIO","NIU","NMR","NNDM","NOAH","NOK","NTCO","NTES","NTZ","NVO","NVS","OCFT","OIBR-C","OIIM","OMAB","ONE","OPRA","ORAN","ORTX","OSN","PAC","PAM","PBR","PBRA","PDD","PHG","PHI","PKX","PLL","PSO","PT","PTNR","PTR","PUK","PUYI","QD","QFIN","QIWI","QK","QTT","RBS","RCEL","RDHL","RDS-A","RDS-B","RDY","REDU","RELX","RENN","RIO","RUHN","RYAAY","RYB","SAN","SAP","SBS","SBSW","SE","SECO","SFET","SFUN","SHG","SHI","SID","SIFY","SIM","SIMO","SKM","SKYS","SMFG","SMMT","SNE","SNN","SNP","SNY","SOGO","SOHU","SOL","SQM","SQNS","SSL","STG","STM","SUPV","SUZ","SY","TAK","TAL","TC","TCOM","TEDU","TEF","TEO","TEVA","TGS","TIGR","TKC","TLC","TLK","TLND","TLSA","TM","TME","TOT","TOUR","TRIB","TRPX","TRVG","TS","TSM","TSU","TTM","TV","TX","UGP","UL","UMC","UN","UXIN","VALE","VEDL","VEON","VIOT","VIPS","VIST","VIV","VJET","VLRS","VNET","VOD","VRNA","WB","WBAI","WBK","WEI","WF","WIMI","WIT","WKEY","WNS","WPP","WSG","WUBA","XIN","XNET","XRF","XTLB","XYF","YI","YIN","YJ","YPF","YRD","YY","ZEAL","ZLAB","ZNH","ZTO"]
    # tickers = ["WBAI","WBK","WEI","WF","WIMI","WIT","WKEY","WNS","WPP","WSG","WUBA","XIN","XNET","XRF","XTLB","XYF","YI","YIN","YJ","YPF","YRD","YY","ZEAL","ZLAB","ZNH","ZTO"]
    # yfin = yahooFinance(tickers, '19700201', '20200527', corp_actions=False, save_in_file=True)
    # tickers = ["KLDW, IBM"]
    # yfin = yahooFinance(tickers, '20200101', '20200517', save_in_file=True )
    # yfin = yahooFinance(tickers, '20200101', '20200115', corp_actions=True )
    # print(yfin.yahoo_API_pandas()['Open']) # if you have save_in_file=False
    # print(yfin.yahoo_API_pandas(path='C:\\Users\\User\\Documents\\yahoo_finance\\'))
    # print(yfin.yahoo_API_pandas())  # corporate actions True/Flase
    # print(yfin.request_yahoo_api('div'))
    # print(yfin.request_yahoo_api('div', path='C:\\Users\\User\\Documents\\yahoo_finance\\'))
    # print(yfin.request_yahoo_api('history', path='C:\\Users\\User\\Documents\\yahoo_finance\\'))
    # print(yfin.request_yahoo_api('history'))
    # print(yfin.request_yahoo_api('split', path='C:\\Users\\User\\Documents\\yahoo_finance\\'))
    # print(yfin.convert_date_to_epoch('20200518'))
    # print(yfin.yahoo_API_python_get_shares_outstanding())
    # print(yfin.yahoo_API_python_get_description(long=True))
    # print(yfin.yahoo_API_python_get_OHLCV('C:\\Users\\User\\Documents\\yahoo_finance\\'))
    # print(yfin.yahoo_API_python_get_OHLCV())
    # print(yfin.yahoo_API_python_get_dividends('C:\\Users\\User\\Documents\\yahoo_finance\\'))
    # print(yfin.yahoo_API_python_get_dividends())
    # print(yfin.yahoo_API_python_get_splits('C:\\Users\\User\\Documents\\yahoo_finance\\'))
    # print(yfin.quandl_python_API(path='C:\\Users\\User\\Documents\\yahoo_finance\\'))
    # print(yfin.yahoo_API_get_shares_outstanding())
    # Get full command-line arguments
    # full_cmd_arguments = sys.argv
    # # Keep all but the first
    # argument_list = full_cmd_arguments[1:]
    # print(argument_list)
    # Initiate the parser
    parser = argparse.ArgumentParser(epilog=" EX: python yahoo_data.py -t IBM FB "
                                            " -s 19900601 -e 20200607  -m OHLCV "
                                            "-f True -p C:\\Users\\User\\Documents\\yahoo_finance\\ ")
    requiredNamed = parser.add_argument_group('required named arguments')
    requiredNamed.add_argument("--ticker_list", "-t", nargs='+', help="provide ticker list: Example IBM GE")
    requiredNamed.add_argument("--start_date", "-s",  help="provide start_date in YYYYMMDD format: Example 20200110")
    requiredNamed.add_argument("--end_date", "-e",  help="provide end_date in YYYYMMDD format: Example 20200510")
    parser.add_argument("--corporate_actions", "-c",  help="True or False, only works with yahoo_API_pandas function (OHLCV_PANDAS) ",default='FALSE')
    parser.add_argument("--save_in_file", "-f",  help="True or False", default='FALSE')
    requiredNamed.add_argument("--function", "-m",  help="choose from this list: OHLCV_PANDAS, DES, SHO  "
                                                  "OHLCV, SPLIT, DIV, OHLCV_YFIN, SPLIT_YFIN, DIV_YFIN, QUANDL_OHLCV, "
                                                  "QUANDL_OTHER")
    parser.add_argument("--path", "-p", help="directory of the file", default='')
    parser.add_argument("--long", "-d", type=bool,  help=" False/True - True is for long description, only  for DES", default=False)
    # parser.add_argument("--event_type", "-o",  help="choose from the following div|history|split, only for request_yahoo_api")
    args = vars(parser.parse_args())
    ticker_list = args['ticker_list']
    start = args['start_date']
    end = args['end_date']
    if args['corporate_actions'].upper() =='TRUE':
        cor_actions = True
    elif args['corporate_actions'].upper() =='FALSE':
        cor_actions = False
    # print(args['save_in_file'].upper())
    if args['save_in_file'].upper() =='TRUE':
        save_file = True
    elif args['save_in_file'].upper() =='FALSE':
        save_file = False
    print(ticker_list)
    funct = args['function']
    path = args['path']
    long = args['long']
    yfin = yahooFinance(ticker_list,start, end, corp_actions=cor_actions, save_in_file=save_file)
    if funct == 'OHLCV_PANDAS':
        yfin.yahoo_API_pandas(path)
    elif funct == 'DES':
        yfin.yahoo_API_python_get_description(long=long)
    elif funct == 'SHO':
        yfin.yahoo_API_get_shares_outstanding(path,)
    elif funct == 'OHLCV':
        yfin.request_yahoo_api('history', path)
    elif funct == 'SPLIT':
        yfin.request_yahoo_api('split', path)
    elif funct == 'DIV':
        yfin.request_yahoo_api('div', path)
    elif funct == 'OHLCV_YFIN':
        yfin.yahoo_API_python_get_OHLCV(path)
    elif funct == 'DIV_YFIN':
        yfin.yahoo_API_python_get_dividends(path)
    elif funct == 'SPLIT_YFIN':
        yfin.yahoo_API_python_get_splits(path)
    elif funct == 'QUANDL_OHLCV':
        yfin.quandl_python_API_OHLCV(path=path, source='WIKI/PRICES')
    elif funct == 'QUANDL_OTHER':
        yfin.quandl_python_API(path=path, source='ZACKS/FC')
    # print(yfin.yah/oo_API_pandas(path='C:\\Users\\User\\Documents\\yahoo_finance\\'))
